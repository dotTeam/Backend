## Backend for dot

php with laravel, restful API based app

### config
All necessary config is in env.example. Just copy env.example to .env

### Docker

```
$ docker-compose up
```
- start backend (redis, php, nginx)

```
$ docker-compose down
```
- stop backend


### Setting up this beauty!

```
$ cp .env.example .env
```
- copy .env.example to .env (don't forget to edit this file according to your needs)

```
$ composer install
```
- install all dependencies for laravel and PHP

```
$ php artisan key:generate
```
- generate a new key for your application

```
$ php artisan jwt:generate
```
- generate a new key for JWT token

```
$ php artisan migrate:refresh --seed
```
- reset current migrations, migrate the database, seed the database

```
$ php artisan optimize --force
$ php artisan route:cache
$ php artisan config:cache
```
- cache route and config (only for production or frontend work)

```
$ php artisan
```
- for a list of all available commands


### Start socket server ###

```
$ redis-server --port 6379
```
- start redis

```
$ node socket.js
```
- start node server

```
$ redis-cli -p 6379 monitor
```
- start monitor utility for intercepting messages


### Database ###

```
$ php artisan migrate
```
- migrate the database

```
$ php artisan migrate:reset
```
- reset all migrations

```
$ php artisan db:seed
```
- seed the database with dummy data