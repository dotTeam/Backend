<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jedrzej\Pimpable\PimpableTrait;

class Module extends Model
{
    use PimpableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'started_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'started_at'];

    public function setUpdatedAt($value)
    {
    }

    public function getUpdatedAtColumn()
    {
    }

    public function Course()
    {
        return $this->belongsTo(Course::class);
    }

    public function Test()
    {
        return $this->hasMany(Test::class);
    }

    public function Homework()
    {
        return $this->hasMany(Homework::class);
    }

    public function PageModule()
    {
        return $this->hasMany(PageModule::class);
    }

    public function Material()
    {
        return $this->hasMany(Material::class);
    }

    public function Discussion()
    {
        return $this->hasMany(Discussion::class);
    }

}
