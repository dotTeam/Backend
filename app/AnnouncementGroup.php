<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnnouncementGroup extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description'];

    public function setUpdatedAt($value)
    {
    }

    public function getUpdatedAtColumn()
    {
    }

    public function Group()
    {
        return $this->belongsTo(Group::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
