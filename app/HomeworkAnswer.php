<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeworkAnswer extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['answer'];

    public function Homework()
    {
        return $this->belongsTo(Homework::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Grade()
    {
        return $this->hasMany(Grade::class);
    }

}
