<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['grade', 'user_id', 'discussion_id', 'test_id', 'homework_id'];

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Discussion()
    {
        return $this->belongsTo(Discussion::class);
    }

    public function Test()
    {
        return $this->belongsTo(Test::class);
    }

    public function HomeworkAnswer()
    {
        return $this->belongsTo(HomeworkAnswer::class);
    }
}
