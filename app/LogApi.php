<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogApi extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['endpoint', 'user_id'];

    public function getUpdatedAtColumn()
    {
        return null;
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
