<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jedrzej\Pimpable\PimpableTrait;

class Test extends Model
{
    use PimpableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'deadline', 'start_date', 'end_date', 'required', 'duration'];

    public function Question()
    {
        return $this->hasMany(Question::class);
    }

    public function Grade()
    {
        return $this->hasMany(Grade::class);
    }

    public function Module()
    {
        return $this->belongsTo(Module::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
