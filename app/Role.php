<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['role'];

    public function setUpdatedAt($value)
    {
    }

    public function getUpdatedAtColumn()
    {
    }

    public function User()
    {
        return $this->hasMany(User::class);
    }

    public function Permission()
    {
        return $this->belongsToMany(Permission::class)->withPivot('if_created_by_auth');
    }
}
