<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentGroup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['message'];

    public function DiscussionGroup()
    {
        return $this->belongsTo(DiscussionGroup::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
