<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    public function setUpdatedAt($value)
    {
    }

    public function getUpdatedAtColumn()
    {
    }

    public function User()
    {
        return $this->hasMany(User::class);
    }

    public function AnnouncementGroup()
    {
        return $this->hasMany(AnnouncementGroup::class);
    }

    public function DiscussionGroup()
    {
        return $this->hasMany(DiscussionGroup::class);
    }

    public function PageGroup()
    {
        return $this->hasMany(PageGroup::class);
    }

}
