<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['message'];

    /**
     * All of the relationships to be touched.
     *
     * @var array
     */
    protected $touches = ['Conversation'];

    public function setUpdatedAt($value)
    {
    }

    public function getUpdatedAtColumn()
    {
    }

    public function Conversation()
    {
        return $this->belongsTo(Conversation::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
