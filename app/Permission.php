<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['route_name', 'description', 'model', 'check'];

    public function User(){
        return $this->belongsToMany(User::class)->withPivot('if_created_by_auth');
    }

    public function Role(){
        return $this->belongsToMany(Role::class)->withPivot('if_created_by_auth');
    }
}
