<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EventUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|required|string|max:60',
            'description' => 'sometimes|required|string',
            'frequency' => 'sometimes|required|integer',
            'start_date' => 'sometimes|required|date|before:end_date',
            'end_date' => 'sometimes|required|date|after:start_date'
        ];
    }
}
