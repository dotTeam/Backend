<?php

namespace App\Http\Requests;

use App\DiscussionGroup;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CommentGroupStoreRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('user_is_in_group', DiscussionGroup::findOrFail($this->discussion_group_id)->Group);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required|string',
            'discussion_group_id' => 'required|integer|exists:discussion_groups,id'
        ];
    }
}
