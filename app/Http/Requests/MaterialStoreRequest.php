<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Module;
use Illuminate\Support\Facades\Gate;

class MaterialStoreRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('teacher_owns_course', Module::findOrFail($this->module_id)->Course);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'upload' => 'required|mimes:' . config('filesystems.material_file_type') . '|max:' . config('filesystems.material_max_file_size'),
            'description' => 'sometimes|required|string',
            'module_id' => 'required|integer|exists:modules,id'
        ];
    }
}
