<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MessageStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required_without_all:conversation_id|exists:users,id',
            'conversation_id' => 'required_without_all:user_id|exists:conversations,id',
            'message' => 'required|string'
        ];
    }
}
