<?php

namespace App\Http\Requests;

use App\Homework;
use App\Http\Requests\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;

class HomeworkAnswerStoreRequest extends Request {

    protected $forbiddenMessage = "Forbidden";

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $homework = Homework::findOrFail($this->homework_id);
        if ($homework->answer_type == 'upload' && ! $this->exists('upload')) {
            $this->forbiddenMessage = 'The upload field is required.';
            return false;
        } elseif (($homework->answer_type == 'text' || $homework->answer_type == 'link') && ! $this->exists('answer')) {
            $this->forbiddenMessage = 'The answer field is required.';
            return false;
        } elseif ($homework->answer_type == 'physical_format') {
            $this->forbiddenMessage = 'Homework must be in physical format.';
            return false;
        }
        if ($homework->deadline < Carbon::now()) {
            $this->forbiddenMessage = 'Deadline ended.';
            return false;
        }
        if ($homework->start_date > Carbon::now()) {
            $this->forbiddenMessage = 'Homework not started yet.';
            return false;
        }
        return Gate::allows('user_take_this_course', $homework->Module->Course);
    }

    public function forbiddenResponse()
    {
        return response()->json(['errors' => $this->forbiddenMessage]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'answer' => 'sometimes|required|string',
            'upload' => 'sometimes|required|mimes:' . config('filesystems.homework_answer_file_type') . '|max:' . config('filesystems.homework_answer_max_file_size'),
            'homework_id' => 'required|integer|exists:homeworks,id'
        ];
    }
}
