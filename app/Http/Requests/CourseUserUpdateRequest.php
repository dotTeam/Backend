<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Course;
use Illuminate\Support\Facades\Gate;

class CourseUserUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('teacher_owns_course', Course::findOrFail($this->route('course')));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role' => 'required|in:teacher,student,assistance'
        ];
    }
}
