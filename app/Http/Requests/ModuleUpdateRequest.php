<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Module;
use Illuminate\Support\Facades\Gate;

class ModuleUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $module = Module::with('Course')->findOrFail($this->route('module'));
        return Gate::allows('teacher_owns_course', $module->Course);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:60',
            'started_at' => 'required|date'
        ];
    }
}
