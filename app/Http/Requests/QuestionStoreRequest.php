<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;
use App\Test;

class QuestionStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('teacher_owns_course', Test::findOrFail($this->test_id)->Module->Course);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question' => 'required|string',
            'description' => 'sometimes|required|string',
            'answer' => 'required|json',
            'correct_answer' => 'required|json',
            'test_id' => 'required|integer|exists:tests,id',
        ];
    }
}
