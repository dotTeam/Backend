<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Test;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;

class QuestionIndexRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $test = Test::findOrFail($this->route()->test);
        return !((! Gate::allows('teacher_owns_course', $test->Module->Course)) && $test->start_date > Carbon::now());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
