<?php

namespace App\Http\Requests;

use App\Discussion;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class DiscussionUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'required|string|max:60',
            'description' => 'sometimes|string',
            'category' => 'required|in:important,normal,arhived',
        ];
    }
}
