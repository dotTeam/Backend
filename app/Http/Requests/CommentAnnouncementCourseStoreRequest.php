<?php

namespace App\Http\Requests;

use App\AnnouncementCourse;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CommentAnnouncementCourseStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('user_take_this_course', AnnouncementCourse::findOrFail($this->announcement_course_id)->Course);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => 'required|string',
            'announcement_course_id' => 'required|integer|exists:announcement_courses,id'
        ];
    }
}
