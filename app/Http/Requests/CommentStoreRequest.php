<?php

namespace App\Http\Requests;

use App\Discussion;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CommentStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('user_take_this_course', Discussion::findOrFail($this->discussion_id)->Module->Course);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => 'required|string',
            'discussion_id' => 'required|integer|exists:discussions,id',
        ];
    }
}
