<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Course;
use Illuminate\Support\Facades\Gate;

class ModuleStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('teacher_owns_course', Course::findOrFail($this->course_id));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:60',
            'course_id' => 'required|integer|exists:courses,id',
            'started_at' => 'required|date'
        ];
    }
}
