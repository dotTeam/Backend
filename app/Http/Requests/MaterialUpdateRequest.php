<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MaterialUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file_name' => 'sometimes|required|string|max:45',
            'description' => 'sometimes|required|string',
        ];
    }
}
