<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\User;

class UserUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|required|string|max:60|min:3',
            'email' => 'sometimes|required|email|unique:users|max:60',
            'password' => 'sometimes|required|max:60|min:3',
            'first_name' => 'sometimes|required|string|max:60',
            'last_name' => 'sometimes|required|string|max:60',
            'registration_number' => 'sometimes|required|integer|unique:users',
            'group_id' => 'sometimes|required|integer|exists:groups,id',
            'role_id' => 'sometimes|required|integer|exists:roles,id',
            'use_gravatar' => 'sometimes|required|boolean',
            'upload' => 'sometimes|required|image|max:2000'
        ];
    }
}
