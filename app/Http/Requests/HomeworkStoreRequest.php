<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Module;
use Illuminate\Support\Facades\Gate;

class HomeworkStoreRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('teacher_owns_course', Module::findOrFail($this->module_id)->Course);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:60',
            'description' => 'sometimes|required|string',
            'start_date' => 'sometimes|required|date|before:end_date',
            'end_date' => 'sometimes|required|date|after:start_date',
            'deadline' => 'sometimes|required|date|after:start_date',
            'points' => 'required|numeric',
            'type' => 'required|in:type1,type2',
            'required' => 'required|boolean',
            'answer_type' => 'required|in:link,text,upload,physical_format',
            'module_id' => 'required|integer|exists:modules,id'
        ];
    }
}
