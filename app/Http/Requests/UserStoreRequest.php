<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:60',
            'email' => 'required|email|unique:users|max:60',
            'password' => 'required|max:60',
            'first_name' => 'required|string|max:60',
            'last_name' => 'required|string|max:60',
            'registration_number' => 'required|integer|unique:users',
            'group_id' => 'sometimes|required|integer|exists:groups,id',
            'role_id' => 'required|integer|exists:roles,id',
            'use_gravatar' => 'sometimes|required|boolean',
            'upload' => 'sometimes|required|image|max:2000'
        ];
    }
}
