<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConversationStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string'
        ];

        foreach($this->request->get('users') as $key => $val)
        {
            $rules['users.'.$key] = 'required|integer|exists:users,id|not_in:'.auth()->user()->id;
        }

        return $rules;
    }
}
