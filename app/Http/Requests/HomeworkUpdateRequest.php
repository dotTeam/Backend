<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeworkUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|required|string|max:60',
            'description' => 'sometimes|required|string',
            'start_date' => 'sometimes|required|date|before:end_date',
            'end_date' => 'sometimes|required|date|after:start_date',
            'deadline' => 'sometimes|required|date|after:start_date',
            'points' => 'sometimes|required|numeric',
            'type' => 'sometimes|required|in:type1,type2',
            'required' => 'sometimes|required|boolean',
            'answer_type' => 'sometimes|required|in:link,text,upload,physical_format',
        ];
    }
}
