<?php

namespace App\Http\Requests;

use App\Homework;
use App\HomeworkAnswer;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class HomeworkAnswerDownloadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $homework_answer = HomeworkAnswer::findOrFail($this->route('homework_answer'));
        return Gate::allows('teacher_owns_course', $homework_answer->Homework->Module->Course) || $homework_answer->user_id == auth()->user()->id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
