<?php

namespace App\Http\Requests;
use App\Group;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class DiscussionGroupStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('user_is_in_group', Group::findOrFail($this->group_id));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:60',
            'group_id' => 'required|integer|exists:groups,id'
        ];
    }
}
