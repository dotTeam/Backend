<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;
use App\Question;

class QuestionUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('teacher_owns_course', Question::findOrFail($this->route()->question)->Test->Module->Course);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question' => 'sometimes|required|string',
            'description' => 'sometimes|required|string',
            'answer' => 'sometimes|required|json',
            'correct_answer' => 'sometimes|required|json',
        ];
    }
}
