<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use MongoClient;

class LogApi {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (env('LOG_DRIVER') == 'mongo') {
            $driver = config('database.connections.mongo.driver');
            $host = config('database.connections.mongo.host');
            $database = config('database.connections.mongo.database');
            $username = config('database.connections.mongo.username');
            $password = config('database.connections.mongo.password');
            $m = new MongoClient($driver + "://${username}:${password}@" + $host);

            $collection = $m->$database->api_endpoints;
            $collection->insert([
                'endpoint' => $request->getPathInfo(),
                'method' => $request->getMethod(),
                'client_ip' => $request->getClientIp(),
                'user_id' => auth()->check() ? auth()->user()->id : "unauthenticated",
                'request' => $request->except('upload'),
                'env' => env('APP_ENV')
            ]);
        }
        return $next($request);
    }
}
