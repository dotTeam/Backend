<?php

namespace App\Http\Middleware;

use Closure;

class Permission {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->route();
        $route_name = $route->getName();
        if (is_null($route_name)) {
            //daca request-ul nu are route name atunci, ii permitem executia !!!
            return $next($request);
        }

        $permission = \App\Permission::where('route_name', $route_name)->first();
        if (is_null($permission) || ! $permission->check) {
            //daca permisiunea nu exista sau daca 'check' nu este true, ii permitem executia !!!
            return $next($request);
        }

        $role = auth()->user()->Role;
        $user_pivot = $permission->User->find(auth()->user()->id);
        $role_pivot = $permission->Role->find($role->id);
        if (is_null($user_pivot) && is_null($role_pivot)) {
            // daca nu gasim legaturi cu role sau user, atunci oprim executia
            return response()->json(['error' => 'Forbidden'], 403);
        }

        $role_user = is_null($user_pivot) ? $role_pivot : $user_pivot;
        if($role_user->pivot->if_created_by_auth){
            // daca este bifat si 'if_created_by_auth' atunci verificam daca modelul respectiv apartine de utilizatorul curent
            $model = '\\App\\' . studly_case($permission->model);
            $model  = new $model;
            $request_model = snake_case($permission->model);
            $model = $model::where('user_id', auth()->user()->id)->find($request->$request_model);
            if(is_null($model)){
                // daca modelul cu id-ul din url nu are un user_id = cu id-ul user-ului inregistrat, oprim executia
                return response()->json(['error' => 'Forbidden'], 403);
            }
        }

        return $next($request);
    }
}
