<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AnnouncementGroup;
use App\Group;

class AnnouncementGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $group = Group::with('AnnouncementGroup')->findOrFail($request->group);
        //$module = Announcement::where('course_id', $request->course)->get();
        return response()->json(['group' => $group]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\AnnouncementGroupStoreRequest $request)
    {
        $announcement = new AnnouncementGroup($request->all());
        $announcement->group_id = $request->group_id;
        $announcement->user_id = auth()->user()->id;
        $announcement->save();
        return response()->json(['announcement' => $announcement]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\AnnouncementGroupUpdateRequest $request, $id)
    {
        $announcement = AnnouncementGroup::findOrFail($id);
        $announcement->update($request->all());
        return response()->json(['announcement' => $announcement]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AnnouncementGroup::findOrFail($id)->delete();
        return response()->json(['message' => 'Announcement deleted successfully']);
    }
}
