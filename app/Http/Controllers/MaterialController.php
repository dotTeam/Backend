<?php

namespace App\Http\Controllers;

use App\Material;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Module;
use Illuminate\Support\Facades\Storage;

class MaterialController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $material = Module::with(['Material' => function($query){
            return $query->pimp();
        }])->findOrFail($request->module);
        return response()->json(['module' => $material]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\MaterialStoreRequest $request)
    {
        $material = new Material($request->all());
        $material->file_name = pathinfo($request->file('upload')->getClientOriginalName(), PATHINFO_FILENAME);
        $material->file_extension = $request->file('upload')->getClientOriginalExtension();
        $material->module_id = $request->module_id;
        $material->user_id = auth()->user()->id;
        $material->save();
        Storage::put(
            'materials/' . $material->id,
            file_get_contents($request->file('upload')->getRealPath())
        );
        return response()->json(['material' => $material]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\MaterialUpdateRequest $request, $id)
    {
        $material = Material::findOrFail($id);
        $material->update($request->all());
        return response()->json(['material' => $material]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $material = Material::findOrFail($request->material);
        $real_path = storage_path('app/materials/' . $material->id);
        if (! file_exists($real_path)) {
            return response()->json(['error' => 'File not found']);
        }
        Storage::delete('materials/' . $material->id);
        $material->delete();
        return response()->json(['message' => 'Material deleted successfully']);
    }

    /**
     * Download the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function download($id)
    {
        $material = Material::findOrFail($id);
        $real_path = storage_path('app/materials/' . $material->id);
        if (! file_exists($real_path)) {
            return response()->json(['error' => 'File not found']);
        }
        $material->increment('download_nr');
        $file_name = $material->file_name . '.' . $material->file_extension;
        return response()->download($real_path, $file_name);
    }
}
