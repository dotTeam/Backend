<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use MongoClient;

class LogController extends Controller {

    private $m;
    private $documents = ['exceptions', 'api_endpoints'];

    public function __construct()
    {
        $driver = config('database.connections.mongo.driver');
        $host = config('database.connections.mongo.host');
        $username = config('database.connections.mongo.username');
        $password = config('database.connections.mongo.password');
        $this->m = new MongoClient($driver + "://${username}:${password}@" + $host);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($document)
    {
        if (env("LOG_DRIVER") != 'mongo') {
            return response()->json(['message' => 'Log driver must be set to mongo.']);
        }
        if (! in_array($document, $this->documents)) {
            return response()->json(['message' => 'You are not allowed to alter ' . strval($document) . ' document.']);
        }
        $json = [];
        parse_str(request()->getQueryString(), $query_array);
        $query_array = array_map('strval', $query_array);
        $query_array = array_map(function($var) {
            return is_numeric($var) ? (int)$var : $var;
        }, $query_array);
        $database = config('database.connections.mongo.database');
        $collection = $this->m->$database->$document;
        $cursor = $collection->find($query_array);
        foreach ($cursor as $doc) {
            $json[] = $doc;
        }
        return response()->json($json);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($document)
    {
        if (env("LOG_DRIVER") != 'mongo') {
            return response()->json(['message' => 'Log driver must be set to mongo.']);
        }
        if (! in_array($document, $this->documents)) {
            return response()->json(['message' => 'You are not allowed to alter ' . strval($document) . ' document.']);
        }
        $database = config('database.connections.mongo.database');
        $this->m->$database->$document->remove();
        return response()->json(['message' => $document . ' deleted successfuly.']);
    }
}
