<?php

namespace App\Http\Controllers;

use App\Forum;
use App\Group;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $forum = Forum::pimp()->simplePaginate();
        return response()->json($forum);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ForumStoreRequest $request)
    {
        $forum = new Forum($request->all());
        $forum->user_id = auth()->user()->id;
        $forum->save();
        return response()->json(['forum' => $forum]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ForumUpdateRequest $request, $id)
    {
        $forum = Forum::findOrFail($id);
        $forum->update($request->all());
        return response()->json(['forum' => $forum]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Forum::findOrFail($id)->delete();
        return response()->json(['message' => 'Forum deleted successfully']);
    }
}
