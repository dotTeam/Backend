<?php

namespace App\Http\Controllers;

use App\HomeworkAnswer;
use App\Homework;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Mews\Purifier\Facades\Purifier;

class HomeworkAnswerController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $homework = Homework::findOrFail($request->homework);
        if (Gate::allows('teacher_owns_course', Homework::findOrFail($request->homework)->Module->Course)) {
            $homework = $homework->load('HomeworkAnswer.User');
        } else {
            $homework = $homework->load(['HomeworkAnswer' => function ($query) {
                $query->where('user_id', auth()->user()->id)->with('User');
            }]);
        }
        return response()->json(['homework' => $homework]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\HomeworkAnswerStoreRequest $request)
    {
        $homework = Homework::findOrFail($request->homework_id);
        $homework_answer = new HomeworkAnswer();
        $homework_answer->user_id = auth()->user()->id;
        $homework_answer->homework_id = $request->homework_id;

        if ($homework->answer_type == 'upload') {
            $homework_answer->file_name = pathinfo($request->file('upload')->getClientOriginalName(), PATHINFO_FILENAME);
            $homework_answer->file_extension = $request->file('upload')->getClientOriginalExtension();
            $homework_answer->save();
            Storage::put(
                'homework_answer/' . $homework_answer->id,
                file_get_contents($request->file('upload')->getRealPath())
            );
        }elseif($homework->answer_type == 'text' || $homework->answer_type == 'link'){
            $homework_answer->answer = Purifier::clean($request->answer);
            $homework_answer->save();
        }
        return response()->json(['homework_answer' => $homework_answer]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $homework_answer = HomeworkAnswer::findOrFail($id);
        if($homework_answer->Homework == 'upload') {
            $real_path = storage_path('app/homework_answer/' . $homework_answer->id);
            if (! file_exists($real_path)) {
                return response()->json(['error' => 'File not found']);
            }
            Storage::delete('homework_answer/' . $homework_answer->id);
        }
        $homework_answer->delete();
        return response()->json(['message' => 'Homework deleted successfully']);
    }

    public function download(Requests\HomeworkAnswerDownloadRequest $request)
    {
        $homework = HomeworkAnswer::findOrFail($request->homework_answer);
        $real_path = storage_path('app/homework_answer/' . $homework->id);
        if (! file_exists($real_path)) {
            return response()->json(['error' => 'File not found']);
        }
        $file_name = $homework->file_name . '.' . $homework->file_extension;
        return response()->download($real_path, $file_name);
    }
}