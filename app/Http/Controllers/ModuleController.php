<?php

namespace App\Http\Controllers;

use App\Course;
use App\Module;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;

class ModuleController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $course = Course::findOrFail($request->course);
        if (auth()->user()->Role->role == 'admin' || Gate::denies('teacher_owns_course', Course::findOrFail($request->course))) {
            $module = $course->load(['Module' => function ($query) {
                return $query->where('started_at', '<', Carbon::createFromTimestamp(time()))->pimp();
            }]);
        } else {
            $module = $course->load(['Module' => function($query){
               return $query->pimp();
            }]);
        }
        //$module = Module::where('course_id', $request->course)->get();
        return response()->json(['course' => $module]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ModuleStoreRequest $request)
    {
        $module = new Module($request->all());
        $module->course_id = $request->course_id;
        $module->save();
        return response()->json(['module' => $module]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ModuleUpdateRequest $request, $id)
    {
        $module = Module::findOrFail($id);
        $module->update($request->all());
        return response()->json(['module' => $module]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $module = Module::with('Course')->with('Material')->findOrFail($id);
        $this->authorize('teacher_owns_course', $module->Course);
        foreach($module->Material as $material){
            $real_path = storage_path('app/materials/' . $material->id);
            if (! file_exists($real_path)) {
                return response()->json(['error' => 'File not found']);
            }
            Storage::delete('materials/' . $material->id);
        }
        $module->delete();
        return response()->json(['message' => 'Module deleted successfully']);
    }
}
