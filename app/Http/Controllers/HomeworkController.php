<?php

namespace App\Http\Controllers;

use App\Course;
use App\Module;
use App\Notification;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Homework;
use Fenos\Notifynder\Facades\Notifynder;
use Fenos\Notifynder\Builder\NotifynderBuilder;

class HomeworkController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $module = Module::with(['Homework' => function($query){
            return $query->pimp();
        }])->findOrFail($request->module);
        return response()->json(['module' => $module]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function user_index()
    {
        $course = Course::whereHas('User', function ($query) {
            $query->where('id', auth()->user()->id);
        })->whereHas('Module.Homework', function ($query) {
            $query->where('start_date', '<', Carbon::now());
        })->with(['Module' => function ($query) {
            $query->whereHas('Homework', function ($query) {
                $query->where('start_date', '<', Carbon::now());
            })->with(['Homework' => function ($query) {
                $query->where('start_date', '<', Carbon::now());
            }]);
        }])->get();
        return response()->json(['course' => $course]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\HomeworkStoreRequest $request)
    {
        $homework = new Homework($request->all());
        $homework->user_id = auth()->user()->id;
        $homework->module_id = $request->module_id;
        $homework->save();

        if (! $request->has('notify') || $request->notify == true) {
            $users = $homework->Module->Course->User;

            Notifynder::loop($users, function(NotifynderBuilder $builder, $user) use ($homework) {
                $builder->category('store.homework')
                    ->from(User::class, auth()->user()->id)
                    ->to(User::class, $user->id)
                    ->extra(['object' => $homework->name])
                    ->url(route('api.v1.user.homework.index'));
            })->sendWithEmail();
        }
        unset($homework->Module);
        return response()->json(['homework' => $homework]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\HomeworkUpdateRequest $request, $id)
    {
        $homework = Homework::findOrFail($id);
        $homework->update($request->all());

        if (! $request->has('notify') || $request->notify == true) {
            $users = $homework->Module->Course->User;
            Notifynder::loop($users, function(NotifynderBuilder $builder, $user) use ($homework) {
                $builder->category('update.homework')
                    ->from(User::class, auth()->user()->id)
                    ->to(User::class, $user->id)
                    ->extra(['object' => $homework->name])
                    ->url(route('api.v1.user.homework.index'));
            })->sendWithEmail();
        }

        unset($homework->Module);
        return response()->json(['homework' => $homework]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Homework::findOrFail($id)->delete();
        return response()->json(['message' => 'Homework deleted successfully']);
    }
}
