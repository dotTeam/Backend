<?php

namespace App\Http\Controllers;

use App\Module;
use App\PageModule;
use Illuminate\Http\Request;
use Fenos\Notifynder\Facades\Notifynder;
use Fenos\Notifynder\Builder\NotifynderBuilder;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mews\Purifier\Facades\Purifier;

class PageModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_module = Module::with(['PageModule' => function ($query) {
            return $query->pimp();
        }])->findOrFail($request->module);
        return response()->json(['module' => $page_module]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\PageModuleStoreRequest $request)
    {
        $request->merge(['content' => Purifier::clean($request->get('content'))]);
        $page_module = new PageModule($request->all());
        $page_module->user_id = auth()->user()->id;
        $page_module->module_id = $request->module_id;
        $page_module->save();

        if (!$request->has('notify') || $request->notify == true) {
            $users = $page_module->Module->Course->User;
            Notifynder::loop($users, function (NotifynderBuilder $builder, $user) use ($page_module) {
                $builder->category('store.module.page')
                    ->from(User::class, auth()->user()->id)
                    ->to(User::class, $user->id)
                    ->extra(['object' => $page_module->name, 'course' => $page_module->Module->Course->name])
                    ->url(route('api.v1.module.page.index', ['module' => $page_module->Module->id]));
            })->sendWithEmail();
        }

        unset($page_module->Module);
        return response()->json(['page_module' => $page_module]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\PageModuleUpdateRequest $request, $id)
    {
        $page_module = PageModule::findOrFail($id);
        if ($request->has('content')) {
            $request->merge(['content' => Purifier::clean($request->get('content'))]);
        }
        $page_module->update($request->all());

        if (!$request->has('notify') || $request->notify == true) {
            $users = $page_module->Module->Course->User;
            Notifynder::loop($users, function (NotifynderBuilder $builder, $user) use ($page_module) {
                $builder->category('update.module.page')
                    ->from(User::class, auth()->user()->id)
                    ->to(User::class, $user->id)
                    ->extra(['object' => $page_module->name, 'course' => $page_module->Module->Course->name])
                    ->url(route('api.v1.module.page.index', ['module' => $page_module->Module->id]));
            })->sendWithEmail();
        }

        unset($page_module->Module);
        return response()->json(['page_module' => $page_module]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        PageModule::findOrFail($request->page_module)->delete();
        return response()->json(['message' => 'Page deleted successfully']);
    }
}
