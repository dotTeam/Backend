<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $group = Group::all();
        //$group = Group::with('User')->get();
        return response()->json(['group' => $group]);
    }

    /**
     * Display a listing of the resource for auth user.
     *
     * @return \Illuminate\Http\Response
     */
    public function user_index(Request $request)
    {
        $group = Group::with('User')->findOrFail($request->group);
        return response()->json(['group' => $group]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\GroupStoreRequest $request)
    {
        $group = Group::create($request->all());
        return response()->json(['group' => $group]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\GroupUpdateRequest $request, $id)
    {
        $group = Group::findOrFail($id);
        $group->update($request->all());
        return response()->json(['group' => $group]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Group::findOrFail($id)->delete();
        return response()->json(['message' => 'Group deleted successfully']);
    }
}
