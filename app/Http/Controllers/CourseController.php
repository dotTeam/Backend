<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use Mews\Purifier\Facades\Purifier;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $course = Course::pimp()->get();
        return response()->json(['course' => $course]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CourseStoreRequest $request)
    {
        $request->merge(['description' => Purifier::clean($request->description)]);
        $course = Course::create($request->all());
        auth()->user()->Course()->attach($course->id, ['role' => 'teacher']);
        return response()->json(['course' => $course]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\CourseUpdateRequest $request, $id)
    {
        $course = Course::findOrFail($id);
        if ($request->has('description')) {
            $request->merge(['description' => Purifier::clean($request->description)]);
        }
        $course->update($request->all());
        return response()->json(['course' => $course]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requests\CourseDeleteRequest $request)
    {
        Course::findOrFail($request->course)->delete();
        return response()->json(['message' => 'Course deleted successfully']);
    }
}
