<?php

namespace App\Http\Controllers;

use App\Course;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class CourseUserController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($cours)
    {
        $cours = Course::with('User')->findOrFail($cours);
        return response()->json(['cours' => $cours]);
    }

    /**
     * Display a listing of the resource for auth user.
     *
     * @return \Illuminate\Http\Response
     */
    public function user_index(Request $request)
    {
        $user_id = $request->has('user_id') ? $request->user_id : auth()->user()->id;
        $user = User::with('Course')->findOrFail($user_id);
        return response()->json(['user' => $user]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CourseUserStoreRequest $request, $course)
    {
        $course = Course::findOrFail($course);
        $user = auth()->user();
        $check_teacher = Gate::allows('teacher_owns_course', $course);
        if ($check_teacher || $user->Role->role == 'admin') {
            $user = User::findOrfail($request->user_id);
            $course->User()->attach($user->id, ['role' => $request->role]);
            $role_msg = $request->role;
        } else {
            $course->User()->attach($user->id, ['role' => 'student']);
            $role_msg = 'student';
        }
        return response()->json(['message' => $user->name . ' became ' . $role_msg . ' for ' . $course->name]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\CourseUserUpdateRequest $request, $course, $user)
    {
        $user = User::findOrFail($user);
        $course = Course::findOrFail($course);
        $user->Course()->updateExistingPivot($course->id, ['role' => $request->role], true);
        return response()->json(['message' => $user->name . ' changed to ' . $request->role . ' for ' . $course->name . '.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($course, $user)
    {
        $course = Course::findOrFail($course);
        $check_teacher = Gate::allows('teacher_owns_course', $course);
        if ($check_teacher || auth()->user()->Role->role == 'admin') {
            $user = User::findOrFail($user);
            $user->Course()->detach($course->id);
        } else {
            $user = auth()->user();
            $user->Course()->detach($course->id);
        }
        return response()->json(['message' => $user->name . ' removed from ' . $course->name . ' successfully.']);
    }
}
