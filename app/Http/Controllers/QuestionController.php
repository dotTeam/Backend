<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Test;
use App\Question;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Requests\QuestionIndexRequest $request)
    {
        $columns = ['question', 'description', 'answer', 'created_at', 'updated_at', 'test_id'];
        $test = Test::findOrFail($request->test);
        if(Gate::allows('teacher_owns_course', $test->Module->Course)){
            $columns[] = 'correct_answer';
        }
        unset($test->Module);
        $test = $test->load(['Question' => function($query) use ($columns){
            return $query->get($columns);
        }]);
        return response()->json(['test' => $test]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\QuestionStoreRequest $request)
    {
        $question = new Question($request->all());
        $question->test_id = $request->test_id;
        $question->save();
        return response()->json(['question' => $question]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\QuestionUpdateRequest $request, $id)
    {
        $question = Question::findOrFail($id);
        $question->update($request->all());
        return response()->json(['question' => $question]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::findOrFail($id);
        $this->authorize('teacher_owns_course', $question->Test->Module->Course);
        $question->delete();
        return response()->json(['message' => 'Question deleted successfully']);
    }
}
