<?php

namespace App\Http\Controllers;

use App\AnnouncementCourse;
use App\CommentAnnouncementCourse;
use Illuminate\Http\Request;
use Mews\Purifier\Facades\Purifier;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CommentAnnouncementCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $announcement_course = AnnouncementCourse::with(['CommentAnnouncementCourse' => function ($query) {
            return $query->pimp();
        }])->findOrFail($request->announcement_course);
        return response()->json(['announcement_course' => $announcement_course]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CommentAnnouncementCourseStoreRequest $request)
    {
        $request->merge(['text' => Purifier::clean($request->text)]);
        $comment_announcement = new CommentAnnouncementCourse($request->all());
        $comment_announcement->user_id = auth()->user()->id;
        $comment_announcement->announcement_course_id = $request->announcement_course_id;
        $comment_announcement->save();
        return response()->json(['comment_announcement' => $comment_announcement]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\CommentAnnouncementCourseUpdateRequest $request, $id)
    {
        $comment_announcement = CommentAnnouncementCourse::findOrFail($id);
        if ($request->has('text')) {
            $request->merge(['text' => Purifier::clean($request->text)]);
        }
        $comment_announcement->update($request->all());
        return response()->json(['comment_announcement' => $comment_announcement]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CommentAnnouncementCourse::findOrFail($id)->delete();
        return response()->json(['message' => 'Comment deleted successfully']);
    }
}
