<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\AnnouncementCourse;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mews\Purifier\Facades\Purifier;

class AnnouncementCourseController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $course = Course::with(['AnnouncementCourse' => function($query){
            return $query->pimp();
        }])->findOrFail($request->course);
        return response()->json(['course' => $course]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\AnnouncementCourseStoreRequest $request)
    {
        $request->merge(['description' => Purifier::clean($request->description)]);
        $announcement = new AnnouncementCourse($request->all());
        $announcement->course_id = $request->course_id;
        $announcement->user_id = auth()->user()->id;
        $announcement->save();
        return response()->json(['announcement' => $announcement]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\AnnouncementCourseUpdateRequest $request, $id)
    {
        $announcement = AnnouncementCourse::findOrFail($id);
        if($request->has('description')){
            $request->merge(['description' => Purifier::clean($request->description)]);
        }
        $announcement->update($request->all());
        return response()->json(['announcement' => $announcement]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AnnouncementCourse::findOrFail($id)->delete();
        return response()->json(['message' => 'Announcement deleted successfully']);
    }
}
