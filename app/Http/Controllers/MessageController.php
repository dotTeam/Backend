<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Message;
use App\Conversation;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MessageController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('conversation_id')) {
            $conversation = Conversation::findOrFail($request->conversation_id);
            $this->authorize('user_is_in_conversation', $conversation);
        }
        elseif ($request->has('user_id')) {
            $conversation = User::userMessage($request->user_id)->first();
            if (is_null($conversation)) {
                return response()->json([]);
            }
        }else{
            return response()->json(['message' => 'conversation_id or user_id is required']);
        }
        $conversation->User()->updateExistingPivot(auth()->user()->id, [], false);
        $message = $conversation->Message()->orderBy('created_at', 'desc')->simplePaginate(10);
        return response()->json($message);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\MessageStoreRequest $request)
    {
        if ($request->has('conversation_id')) {
            $conversation = Conversation::findOrFail($request->conversation_id);
            $this->authorize('user_is_in_conversation', $conversation);
        }
        elseif ($request->has('user_id')) {
            $conversation = User::userMessage($request->user_id)->first();
            if (is_null($conversation)) {
                $conversation = Conversation::create();
                $conversation->User()->attach($request->user_id);
                $conversation->User()->attach(auth()->user()->id);
            }
        }else{
            response()->json(['message' => 'conversation_id or user_id is required']);
        }
        $message = new Message($request->all());
        $message->user_id = auth()->user()->id;
        $conversation->Message()->save($message);
        return response()->json(['message' => $message]);
    }
}
