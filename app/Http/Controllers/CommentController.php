<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Discussion;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $comment = Discussion::with(['Comment' => function($query){
            return $query->pimp();
        }])->findOrFail($request->discussion);
        //$comment = Comment::where('discussion_id', $request->discussion)->get();
        return response()->json(['discussion' => $comment]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CommentStoreRequest $request)
    {
        $comment = new Comment($request->all());
        $comment->user_id = auth()->user()->id;
        $comment->discussion_id = $request->discussion_id;
        $comment->save();
        return response()->json(['comment' => $comment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\CommentUpdateRequest $request, $id)
    {
        $comment = Comment::findOrFail($id);
        $comment->update($request->all());
        return response()->json(['comment' => $comment]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Comment::findOrFail($request->comment)->delete();
        return response()->json(['message' => 'Comment deleted successfully']);
    }
}
