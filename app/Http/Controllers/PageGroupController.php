<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\PageGroup;
use App\Http\Controllers\Controller;
use Mews\Purifier\Facades\Purifier;

class PageGroupController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $group = Group::with('PageGroup')->findOrFail($request->group);
        //$module = Module::where('course_id', $request->course)->get();
        return response()->json(['group' => $group]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\PageGroupStoreRequest $request)
    {
        $request->merge(['content' => Purifier::clean($request->get('content'))]);
        $page = new PageGroup($request->all());
        $page->user_id = auth()->user()->id;
        $page->group_id = $request->group_id;
        $page->save();
        return response()->json(['page' => $page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\PageGroupUpdateRequest $request, $id)
    {
        $page = PageGroup::findOrFail($id);
        if($request->has('content')) {
            $request->merge(['content' => Purifier::clean($request->get('content'))]);
        }
        $page->update($request->all());
        return response()->json(['page' => $page]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PageGroup::findOrFail($id)->delete();
        return response()->json(['message' => 'Page deleted successfully']);
    }
}
