<?php

namespace App\Http\Controllers;

use App\Course;
use App\Group;
use App\User;
use Illuminate\Http\Request;
use App\Event;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Fenos\Notifynder\Facades\Notifynder;
use Fenos\Notifynder\Builder\NotifynderBuilder;

class EventController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::whereHas('Users', function ($query) {
            $query->where('user_id', auth()->user()->id);
        })->orWhere('user_id', auth()->user()->id)->get();
        return response()->json(['event' => $events]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\EventStoreRequest $request)
    {
        $event = new Event($request->all());
        $event->user_id = auth()->user()->id;
        $event->save();
        if (auth()->user()->Role->role == 'teacher' || auth()->user()->Role->role == 'admin') {
            if ($request->has('group_id')) {
                $users = Group::findOrFail($request->group_id)->User;
                $event->Users()->attach($users->lists(['id'])->toArray());
            }
            if ($request->has('course_id')) {
                $course = Course::findOrFail($request->course_id);
                $this->authorize('teacher_owns_course', $course);
                $users = $course->User;
                $event->Users()->attach($users->lists(['id'])->toArray());
            }
            if (($request->has('course_id') || $request->has('group_id')) && (! $request->has('notify') || $request->notify == true)) {
                Notifynder::loop($users, function (NotifynderBuilder $builder, $user) use ($event) {
                    $builder->category('store.event')
                        ->from(User::class, auth()->user()->id)
                        ->to(User::class, $user->id)
                        ->extra(['object' => $event->name])
                        ->url(route('api.v1.event.index'));
                })->sendWithEmail();
            }
        }
        /*if($request->has('for_user_id')){
            $users = User::findOrFail($request->for_user_id);
            $event->Users()->attach($users->id);
        }*/
        return response()->json(['event' => $event]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(Requests\EventUpdateRequest $request, $id)
    {
        $event = Event::findOrFail($id);
        $event->update($request->all());
        // TODO pune notify si la update
        return response()->json(['event' => $event]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        $event = Event::findOrFail($id);
        if ($event->user_id == auth()->user()->id) {
            $event->delete();
        } else {
            $event->Users()->dettach(auth()->user()->id);
        }
        return response()->json(['message' => 'Event deleted successfully']);
    }
}
