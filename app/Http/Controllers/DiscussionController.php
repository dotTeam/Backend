<?php

namespace App\Http\Controllers;

use App\Module;
use App\Discussion;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DiscussionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $discussion = Module::with(['Discussion' => function ($query) {
            return $query->pimp();
        }])->findOrFail($request->module);
        //$discussion = Discussion::where('course_id', $request->course)->get();
        return response()->json(['module' => $discussion]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\DiscussionStoreRequest $request)
    {
        $discussion = new Discussion($request->all());
        $discussion->user_id = auth()->user()->id;
        $discussion->module_id = $request->module_id;
        $discussion->save();
        return response()->json(['discussion' => $discussion]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\DiscussionUpdateRequest $request, $id)
    {
        $discussion = Discussion::findOrFail($id);
        $discussion->update($request->all());
        return response()->json(['discussion' => $discussion]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Discussion::findOrFail($request->discussion)->delete();
        return response()->json(['message' => 'Discussion deleted successfully']);
    }
}
