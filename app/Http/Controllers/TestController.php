<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Test;
use App\Module;
use App\Course;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $module = Module::findOrFail($request->module);
        if (Gate::allows('teacher_owns_course', $module->Course)) {
            unset($module->Course);
            $module = $module->load(['Test' => function ($query) {
                return $query->pimp();
            }]);
        } else {
            unset($module->Course);
            $module->load(['Test' => function ($query) {
                return $query->where('start_date', '<', Carbon::now());
            }]);
        }
        return response()->json(['module' => $module]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function user_index()
    {
        $course = Course::whereHas('User', function ($query) {
            $query->where('id', auth()->user()->id);
        })->whereHas('Module.Test', function ($query) {
            $query->where('start_date', '<', Carbon::now());
        })->with(['Module' => function ($query) {
            $query->whereHas('Test', function ($query) {
                $query->where('start_date', '<', Carbon::now());
            })->with(['Test' => function ($query) {
                $query->where('start_date', '<', Carbon::now());
            }]);
        }])->get();
        return response()->json(['course' => $course]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\TestStoreRequest $request)
    {
        $test = new Test($request->all());
        $test->user_id = auth()->user()->id;
        $test->module_id = $request->module_id;
        $test->save();
        return response()->json(['test' => $test]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\TestUpdateRequest $request, $id)
    {
        $test = Test::findOrFail($id);
        $test->update($request->all());
        return response()->json(['test' => $test]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Test::findOrFail($id)->delete();
        return response()->json(['message' => 'Test deleted successfully']);
    }
}
