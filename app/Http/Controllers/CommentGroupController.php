<?php

namespace App\Http\Controllers;

use App\CommentGroup;
use App\DiscussionGroup;
use Illuminate\Http\Request;
use Mews\Purifier\Facades\Purifier;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CommentGroupController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $discussion_group = DiscussionGroup::with('CommentGroup')->findOrFail($request->discussion_group);
        //$module = Module::where('course_id', $request->course)->get();
        return response()->json(['discussion_group' => $discussion_group]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CommentGroupStoreRequest $request)
    {
        $request->merge(['message' => Purifier::clean($request->message)]);
        $comment_group = new CommentGroup($request->all());
        $comment_group->user_id = auth()->user()->id;
        $comment_group->discussion_group_id = $request->discussion_group_id;
        $comment_group->save();
        return response()->json(['comment_group' => $comment_group]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\CommentGroupUpdateRequest $request, $id)
    {
        $comment_group = CommentGroup::findOrFail($id);
        if($request->has('message')){
            $request->merge(['message' => Purifier::clean($request->message)]);
        }
        $comment_group->update($request->all());
        return response()->json(['comment_group' => $comment_group]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CommentGroup::findOrFail($id)->delete();
        return response()->json(['message' => 'Comment deleted successfully']);
    }
}
