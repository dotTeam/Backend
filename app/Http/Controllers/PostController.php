<?php

namespace App\Http\Controllers;

use App\Forum;
use App\Post;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*$post = Forum::with(['Post' => function ($query) {
            return $query->pimp();
        }])->findOrFail($request->forum);*/
        //return response()->json(['forum' => $post]);
        $post = Post::where('forum_id', $request->forum)->pimp()->simplePaginate();
        return response()->json($post);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\PostStoreRequest $request)
    {
        $post = new Post($request->all());
        $post->user_id = auth()->user()->id;
        $post->forum_id = $request->forum_id;
        $post->save();
        return response()->json(['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\PostUpdateRequest $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->update($request->all());
        return response()->json(['post' => $post]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::findOrFail($id)->delete();
        return response()->json(['message' => 'Post deleted successfully']);
    }
}
