<?php

namespace App\Http\Controllers;

use App\DiscussionGroup;
use App\Group;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DiscussionGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $group = Group::with('DiscussionGroup')->findOrFail($request->group);
        //$module = Module::where('course_id', $request->course)->get();
        return response()->json(['group' => $group]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\DiscussionGroupStoreRequest $request)
    {
        $discussion_group = new DiscussionGroup($request->all());
        $discussion_group->user_id = auth()->user()->id;
        $discussion_group->group_id = $request->group_id;
        $discussion_group->save();
        return response()->json(['discussion_group' => $discussion_group]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\DiscussionGroupUpdateRequest $request, $id)
    {
        $discussion_group = DiscussionGroup::findOrFail($id);
        $discussion_group->update($request->all());
        return response()->json(['discussion_group' => $discussion_group]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DiscussionGroup::findOrFail($id)->delete();
        return response()->json(['message' => 'Discussion deleted successfully']);
    }
}
