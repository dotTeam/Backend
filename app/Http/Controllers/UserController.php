<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::with('Role')->with('Group')->get();
        return response()->json(['user' => $user]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_profile_img($user = null)
    {
        $user = is_null($user) ? auth()->user() : User::findOrFail($user);
        if ($user->use_gravatar) {
            $profile_img = @file_get_contents("http://www.gravatar.com/avatar/" . md5(strtolower(trim($user->email))) . "?d=404");
            if ($profile_img != FALSE) {
                return response($profile_img, 200)->header('Content-Type', 'image/png');
            }
        } else {
            $real_path = storage_path('app/profile_img/') . $user->id;
            if (file_exists($real_path)) {
                $profile_img = Storage::get('profile_img/' . $user->id);
                $content_type = image_type_to_mime_type(exif_imagetype($real_path));
                return response($profile_img, 200)->header('Content-Type', $content_type);
            }
        }
        $profile_img = Storage::get('default_profile_img/' . $user->id % 9 . '.jpg');
        return response($profile_img, 200)->header('Content-Type', 'image/jpeg');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\UserStoreRequest $request)
    {
        $user = new User($request->all());
        $user->role_id = $request->role_id;
        $user->group_id = $request->group_id;
        $user->password = Hash::make($request->password);
        $user->save();
        if ($request->hasFile('upload') && $request->file('upload')->isValid()) {
            $request->file('upload')->move(storage_path('app/profile_img'), $user->id);
        }
        return response()->json(['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UserUpdateRequest $request, $id = null)
    {
        if (auth()->user()->Role->role == 'admin' && ! is_null($id)) {
            $user = User::findOrFail($id);
            $user->role_id = $request->has('role_id') ? $request->role_id : $user->role_id;
            $user->group_id = $request->has('group_id') ? $request->group_id : $user->group_id;
        } else {
            $user = auth()->user();
        }
        $user->fill($request->all());
        $user->password = $request->has('password') ? Hash::make($request->password) : $user->password;
        $user->save();
        if ($request->hasFile('upload') && $request->file('upload')->isValid()) {
            $request->file('upload')->move(storage_path('app/profile_img'), $user->id);
        }
        return response()->json(['user' => $user]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = null)
    {
        if (auth()->user()->Role->role == 'admin' && ! is_null($id)) {
            $user = User::findOrFail($id);
        } else {
            $user = auth()->user();
        }
        /*$real_path = storage_path('app/profile_img/') . $user->id;
        if (file_exists($real_path)) {
            Storage::delete('profile_img/' . $user->id);
        }*/
        $user->delete();
        return response()->json(['message' => 'User deleted successfully']);
    }
}
