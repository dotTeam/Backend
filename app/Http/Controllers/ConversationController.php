<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Conversation;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class ConversationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conversations = Conversation::whereHas('User', function ($query) {
            $query->where('user_id', auth()->user()->id);
        })->with('User')->get();
        return response()->json(['conversation' => $conversations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ConversationStoreRequest $request)
    {
        if (count($request->users) < 2) {
            return response()->json(['error' => 'Please add more than one user.']);
        }
        $conversation = new Conversation();
        $conversation->fill($request->all())->save();
        $conversation->User()->attach($request->users);
        $conversation->User()->attach(auth()->user()->id);
        return response()->json(['conversation' => $conversation]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ConversationUpdateRequest $request, $id)
    {
        $conversation = Conversation::findOrFail($id);
        $conversation->update($request->all());
        return response()->json(['conversation' => $conversation]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $conversation = Conversation::findOrFail($id);
        $this->authorize('user_is_in_conversation', $conversation);
        $conversation->User()->detach(auth()->user()->id);
        return response()->json(['message' => 'Conversation deleted successfully']);
    }
}
