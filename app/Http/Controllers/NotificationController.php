<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Fenos\Notifynder\Facades\Notifynder;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $json = [];
        $json['notification'] = Notifynder::getAll(auth()->user()->id, 10, false);
        $json['count_not_read'] = Notifynder::countNotRead(auth()->user()->id);
        Notifynder::readAll(auth()->user()->id);
        return response()->json(['notification' => $json]);
    }

    public function notification_count()
    {
        $notification_count = Notifynder::countNotRead(auth()->user()->id);
        return response()->json(['notification_count' => $notification_count]);
    }
}
