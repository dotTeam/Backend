<?php

namespace App\Http\Controllers;

use App\QuestionAnswer;
use App\User;
use Illuminate\Http\Request;
use App\Test;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class QuestionAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $test = Test::findOrFail($request->test);
        $this->authorize('teacher_owns_course', $test->Module->Course);
        $answers = User::whereHas('QuestionAnswer.Question', function ($query) use ($test) {
            return $query->where('test_id', $test->id);
        })->with(['QuestionAnswer' => function ($query) use ($test) {
            return $query->whereHas('Question', function ($query) use ($test) {
                return $query->where('test_id', $test->id);
            })->with(['Question' => function ($query) use ($test) {
                return $query->where('test_id', $test->id);
            }]);
        }])->get();
        return response()->json(['answers' => $answers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->json(['response_test' => $request->all()]);
    }
}
