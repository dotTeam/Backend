<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'api/v1'], function () {

    post('login', 'Auth\AuthController@authenticate');

    post('password/email', 'Auth\PasswordController@postEmail');
    post('password/reset', 'Auth\PasswordController@postReset');

    // in grupul de mai jos se pun doar endpoint-urile care au nevoie de token (autentificare inainte)
    Route::group(['middleware' => ['jwt.auth', 'log.api', 'permission']], function () {

        // Forum
        Route::resource('forum', 'ForumController', ['except' => ['create', 'show', 'edit']]);

        get('forum/{forum}/post', ['as' => 'api.v1.forum.post.index', 'uses' => 'PostController@index']);
        Route::resource('post', 'PostController', ['except' => ['create', 'show', 'edit']]);

        // Group
        get('group/{group}/user', ['as' => 'api.v1.group.user.index', 'uses' => 'GroupController@user_index']);
        Route::resource('group', 'GroupController', ['except' => ['create', 'show', 'edit']]);

        get('group/{group}/page', ['as' => 'api.v1.group.page.index', 'uses' => 'PageGroupController@index']);
        Route::resource('page-group', 'PageGroupController', ['except' => ['index', 'create', 'show', 'edit']]);

        get('group/{group}/discussion', ['as' => 'api.v1.group.discussion-group.index', 'uses' => 'DiscussionGroupController@index']);
        Route::resource('discussion-group', 'DiscussionGroupController', ['except' => ['index', 'create', 'show', 'edit']]);

        get('discussion-group/{discussion_group}/comment', ['as' => 'api.v1.discussion-group.comment-group.index', 'uses' => 'CommentGroupController@index']);
        Route::resource('comment-group', 'CommentGroupController', ['except' => ['index', 'create', 'show', 'edit']]);

        get('group/{group}/announcement-group', ['as' => 'api.v1.announcement-group.index', 'uses' => 'AnnouncementGroupController@index']);
        Route::resource('announcement-group', 'AnnouncementGroupController', ['except' => ['index', 'create', 'show', 'edit']]);

        // Cours
        Route::resource('course', 'CourseController', ['except' => ['create', 'show', 'edit']]);

        get('course/{course}/announcement-course', ['as' => 'api.v1.announcement-course.index', 'uses' => 'AnnouncementCourseController@index']);
        Route::resource('announcement-course', 'AnnouncementCourseController', ['except' => ['index', 'create', 'show', 'edit']]);

        get('announcement-course/{announcement_course}/comment', ['as' => 'api.v1.announcement-course.comment.index', 'uses' => 'CommentAnnouncementCourseController@index']);
        Route::resource('comment-announcement-course', 'CommentAnnouncementCourseController', ['except' => ['index', 'create', 'show', 'edit']]);

        get('user/course', ['as' => 'api.v1.user.course.index', 'uses' => 'CourseUserController@user_index']);
        Route::resource('course.user', 'CourseUserController', ['except' => ['create', 'show', 'edit']]);

        get('course/{course}/module', ['as' => 'api.v1.course.module.index', 'uses' => 'ModuleController@index']);
        Route::resource('module', 'ModuleController', ['except' => ['index', 'create', 'show', 'edit']]);

        get('module/{module}/page', ['as' => 'api.v1.module.page.index', 'uses' => 'PageModuleController@index']);
        Route::resource('page-module', 'PageModuleController', ['except' => ['index', 'create', 'show', 'edit']]);

        get('module/{module}/material', ['as' => 'api.v1.module.material.index', 'uses' => 'MaterialController@index']);
        get('material/{material}', ['as' => 'api.v1.module.material.download', 'uses' => 'MaterialController@download']);
        Route::resource('material', 'MaterialController', ['except' => ['index', 'create', 'show', 'edit']]);

        get('module/{module}/discussion', ['as' => 'api.v1.module.discussion.index', 'uses' => 'DiscussionController@index']);
        Route::resource('discussion', 'DiscussionController', ['except' => ['index', 'create', 'show', 'edit']]);

        get('discussion/{discussion}/comment', ['as' => 'api.v1.discussion.comment.index', 'uses' => 'CommentController@index']);
        Route::resource('comment', 'CommentController', ['except' => ['index', 'create', 'show', 'edit']]);

        // Test
        get('user/test', ['as' => 'api.v1.user.test.index', 'uses' => 'TestController@user_index']);
        get('module/{module}/test', ['as' => 'api.v1.module.test.index', 'uses' => 'TestController@index']);
        Route::resource('test', 'TestController', ['except' => ['index', 'create', 'show', 'edit']]);

        // Question
        get('test/{test}/question', ['as' => 'api.v1.module.question.index', 'uses' => 'QuestionController@index']);
        Route::resource('question', 'QuestionController', ['except' => ['index', 'create', 'show', 'edit']]);

        // Question answer
        get('test/{test}/question-answer', ['as' => 'api.v1.test.question-answer.index', 'uses' => 'QuestionAnswerController@index']);
        post('question-answer', ['as' => 'api.v1.question-answer.store', 'uses' => 'QuestionAnswerController@store']);

        // Homework
        get('user/homework', ['as' => 'api.v1.user.homework.index', 'uses' => 'HomeworkController@user_index']);
        get('module/{module}/homework', ['as' => 'api.v1.module.homework.index', 'uses' => 'HomeworkController@index']);
        Route::resource('homework', 'HomeworkController', ['except' => ['index', 'create', 'show', 'edit']]);

        // Homework Answer
        get('homework/{homework}/homework-answer', ['as' => 'api.v1.homework.homework-answer.index', 'uses' => 'HomeworkAnswerController@index']);
        get('homework-answer/{homework_answer}', ['as' => 'api.v1.homework.homework-answer.download', 'uses' => 'HomeworkAnswerController@download']);
        Route::resource('homework-answer', 'HomeworkAnswerController', ['except' => ['index', 'create', 'update', 'show', 'edit']]);

        // Conversation
        Route::resource('conversation', 'ConversationController', ['except' => ['create', 'show', 'edit']]);

        // Message
        get('message', ['as' => 'api.v1.message.index', 'uses' => 'MessageController@index']);
        post('message', ['as' => 'api.v1.message.store', 'uses' => 'MessageController@store']);

        // User
        get('user', ['as' => 'api.v1.user.index', 'uses' => 'UserController@index']);
        get('user/profile-img/{user?}', ['as' => 'api.v1.user.profile-img.index', 'uses' => 'UserController@index_profile_img']);
        post('user', ['as' => 'api.v1.user.store', 'uses' => 'UserController@store']);
        put('user/{user?}', ['as' => 'api.v1.user.update', 'uses' => 'UserController@update']);
        delete('user/{user?}', ['as' => 'api.v1.user.destroy', 'uses' => 'UserController@destroy']);
        //Route::resource('user', 'UserController', ['except' => ['create', 'show', 'edit']]);

        // Event
        Route::resource('event', 'EventController', ['except' => ['create', 'show', 'edit']]);

        // Role
        get('role', ['as' => 'api.v1.role.index', 'uses' => 'RoleController@index']);

        // Log
        get('log/{document}', ['as' => 'api.v1.log.index', 'uses' => 'LogController@index']);
        delete('log/{document}', ['as' => 'api.v1.log.delete', 'uses' => 'LogController@destroy']);

        // Notification
        get('notification', ['as' => 'api.v1.notification.index', 'uses' => 'NotificationController@index']);
        get('notification-count', ['as' => 'api.v1.notification-count.index', 'uses' => 'NotificationController@notification_count']);
    });
});

get('/', ['as' => 'api.v1.home.index', 'uses' => 'HomeController@index']);