<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract {
    use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'first_name', 'last_name', 'registration_number', 'use_gravatar'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function scopeUserMessage($query, $user_id)
    {
        return $query->findOrFail($user_id)->Conversation()->whereHas('User', function ($query) {
            $query->where('user_id', auth()->user()->id);
        })->has('user', 2);
    }

    public function Grade()
    {
        return $this->hasMany(Grade::class);
    }

    public function PageGroup()
    {
        return $this->hasMany(PageGroup::class);
    }

    public function Post()
    {
        return $this->hasMany(Post::class);
    }

    public function AnnouncementGroup()
    {
        return $this->hasMany(AnnouncementGroup::class);
    }

    public function AnnouncementCourse()
    {
        return $this->hasMany(AnnouncementCourse::class);
    }

    public function CommentAnnouncementCourse()
    {
        return $this->hasMany(CommentAnnouncementCourse::class);
    }

    public function Forum()
    {
        return $this->hasMany(Forum::class);
    }

    public function DiscussionGroup()
    {
        return $this->hasMany(DiscussionGroup::class);
    }

    public function CommentGroup()
    {
        return $this->hasMany(CommentGroup::class);
    }

    public function Events()
    {
        return $this->belongsToMany(Event::class);
    }

    public function Event()
    {
        return $this->hasMany(Event::class);
    }

    public function LogApi()
    {
        return $this->hasMany(LogApi::class);
    }

    public function Comment()
    {
        return $this->hasMany(Comment::class);
    }

    public function Discussion()
    {
        return $this->hasMany(Discussion::class);
    }

    public function PageModule()
    {
        return $this->hasMany(PageModule::class);
    }

    public function Material()
    {
        return $this->hasMany(Material::class);
    }

    public function Group()
    {
        return $this->belongsTo(Group::class);
    }

    public function Role()
    {
        return $this->belongsTo(Role::class);
    }

    public function Permission()
    {
        return $this->belongsToMany(Permission::class)->withPivot('if_created_by_auth');
    }

    public function Course()
    {
        return $this->belongsToMany(Course::class)->withPivot('role')->withTimestamps();
    }

    public function Conversation()
    {
        return $this->belongsToMany(Conversation::class)->withTimestamps();
    }

    public function Homework()
    {
        return $this->hasMany(Homework::class);
    }

    public function HomeworkAnswer()
    {
        return $this->hasMany(HomeworkAnswer::class);
    }

    public function Test()
    {
        return $this->hasMany(Test::class);
    }

    public function QuestionAnswer()
    {
        return $this->hasMany(QuestionAnswer::class);
    }
}
