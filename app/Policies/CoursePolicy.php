<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class CoursePolicy {
    use HandlesAuthorization;

    public function teacher_owns_course($user, $course)
    {
        $check_teacher = $course->User->where('pivot.user_id', $user->id)->where('pivot.role', 'teacher');
        return ! $check_teacher->isEmpty();
    }

    public function user_take_this_course($user, $course)
    {
        $check_user = $course->User->where('pivot.user_id', $user->id);
        return ! $check_user->isEmpty();
    }
}
