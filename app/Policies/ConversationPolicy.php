<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class ConversationPolicy
{
    use HandlesAuthorization;

    public function user_is_in_conversation($user, $conversation)
    {
        $check_user = $conversation->User->where('id', $user->id);
        return ! $check_user->isEmpty();
    }
}
