<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy {
    use HandlesAuthorization;

    public function user_is_in_group($user, $group)
    {
        $check_user = $group->User->where('id', $user->id);
        return ! $check_user->isEmpty();
    }

    public function teacher_is_in_group($user, $group)
    {
        $check_user = $group->User->where('id', $user->id)->Role->where('role', 'teacher');
        return ! $check_user->isEmpty();
    }

}
