<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'frequency', 'start_date', 'end_date'];

    public function Users()
    {
        return $this->belongsToMany(User::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
