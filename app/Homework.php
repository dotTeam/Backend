<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jedrzej\Pimpable\PimpableTrait;

class Homework extends Model
{
    use PimpableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'start_date', 'end_date', 'deadline', 'points', 'type', 'required', 'answer_type'];

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Module()
    {
        return $this->belongsTo(Module::class);
    }

    public function HomeworkAnswer()
    {
        return $this->hasMany(HomeworkAnswer::class);
    }

}
