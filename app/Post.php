<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jedrzej\Pimpable\PimpableTrait;

class Post extends Model
{
    use PimpableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['message'];

    public $searchable = ['id', 'message', 'user_id', 'forum_id', 'created_at', 'updated_at'];

    public function Forum()
    {
        return $this->belongsTo(Forum::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
