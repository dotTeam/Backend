<?php

namespace App\Notify;

use App\User;
use Fenos\Notifynder\Contracts\Sender;
use Fenos\Notifynder\Contracts\NotifynderSender;
use Fenos\Notifynder\Facades\Notifynder;
use Illuminate\Mail\Mailer;
use App\Events\NotifyUserEvent;

class EmailNotificationSender implements Sender {

    protected $notifications;

    protected $mailer;

    public function __construct($notifications, Mailer $mailer)
    {
        $this->notifications = $notifications;
        $this->mailer = $mailer;
    }

    public function send(NotifynderSender $sender)
    {
        $notificationSent = [];
        foreach ($this->notifications as $notification) {
            $notification = $sender->sendOne($notification);
            $notificationSent[] = Notifynder::getNotRead($notification->to_id, 1)[0];
        }

        $this->sendEmails($notificationSent);
    }

    public function sendEmails($notificationsSent)
    {
        foreach ($notificationsSent as $notification) {
            $notification = $notification->toArray();
            event(new NotifyUserEvent($notification));
            $user = User::findOrFail($notification['to_id']);
            $notification['user'] = $user;
            $this->mailer->queue('emails.notification', $notification, function ($message) use ($user) {
                return $message->from('dot@app.com', 'Dot team')
                    ->to($user->email, $user->name)->subject('Email Notification');
            });
        }
    }
}