<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jedrzej\Pimpable\PimpableTrait;

class AnnouncementCourse extends Model
{
    use PimpableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description'];
    public function setUpdatedAt($value)
    {
    }

    public function getUpdatedAtColumn()
    {
    }

    public function Course()
    {
        return $this->belongsTo(Course::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function CommentAnnouncementCourse()
    {
        return $this->hasMany(CommentAnnouncementCourse::class);
    }
}
