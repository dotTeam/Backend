<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jedrzej\Pimpable\PimpableTrait;

class CommentAnnouncementCourse extends Model
{
    use PimpableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['text'];

    public function AnnouncementCourse()
    {
        return $this->belongsTo(AnnouncementCourse::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
