<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscussionGroup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function setUpdatedAt($value)
    {
    }

    public function getUpdatedAtColumn()
    {
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Group()
    {
        return $this->belongsTo(Group::class);
    }

    public function CommentGroup()
    {
        return $this->hasMany(CommentGroup::class);
    }
}
