<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jedrzej\Pimpable\PimpableTrait;

class Material extends Model
{
    use PimpableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'file_name'];

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Module()
    {
        return $this->belongsTo(Module::class);
    }
}
