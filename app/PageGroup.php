<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageGroup extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'content'];

    public function Group()
    {
        return $this->belongsTo(Group::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
