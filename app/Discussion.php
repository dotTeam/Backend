<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jedrzej\Pimpable\PimpableTrait;

class Discussion extends Model
{
    use PimpableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['subject', 'description', 'category'];

    public function setUpdatedAt($value)
    {
    }

    public function getUpdatedAtColumn()
    {
    }

    public function Comment()
    {
        return $this->hasMany(Comment::class);
    }

    public function Grade()
    {
        return $this->hasMany(Grade::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Module()
    {
        return $this->belongsTo(Module::class);
    }
}
