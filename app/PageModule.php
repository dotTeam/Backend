<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jedrzej\Pimpable\PimpableTrait;

class PageModule extends Model
{
    use PimpableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'content'];

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Module()
    {
        return $this->belongsTo(Module::class);
    }
}
