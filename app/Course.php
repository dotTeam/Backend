<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jedrzej\Pimpable\PimpableTrait;

class Course extends Model
{
    use PimpableTrait;

    protected $fillable = ['name', 'description'];

    public function User()
    {
        return $this->belongsToMany(User::class)->withPivot('role')->withTimestamps();
    }

    public function Module()
    {
        return $this->hasMany(Module::class);
    }

    public function AnnouncementCourse()
    {
        return $this->hasMany(AnnouncementCourse::class);
    }
}
