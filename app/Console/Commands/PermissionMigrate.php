<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Permission;

class PermissionMigrate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate permissions with seeder permission defined in application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return $this->call('db:seed', ['--class' => 'PermissionTableSeeder']);
        //$this->call(\PermissionTableSeeder::class);
        /*$routes = \Illuminate\Support\Facades\Route::getRoutes();
        foreach($routes as $route){
            $permission = Permission::create(['route_name' => $route->getName(), 'description' => 'Some description ...']);
        }*/
    }
}
