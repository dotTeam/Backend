<?php

namespace App\Console\Commands;

use App\Permission;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PermissionReset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset all permissions. Truncate table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('permission_role')->truncate();
        DB::table('permission_user')->truncate();
        DB::table('permissions')->delete();
    }
}
