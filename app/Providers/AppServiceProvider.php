<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Fenos\Notifynder\Facades\Notifynder;
use App\Notify\EmailNotificationSender;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Notifynder::extend('sendWithEmail', function ($notifications, $app) {
            return new EmailNotificationSender($notifications, $app['mailer']);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
