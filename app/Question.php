<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['question', 'description', 'answer', 'correct_answer'];

    public function Test()
    {
        return $this->belongsTo(Test::class);
    }

    public function QuestionAnswer()
    {
        return $this->hasMany(QuestionAnswer::class);
    }
}
