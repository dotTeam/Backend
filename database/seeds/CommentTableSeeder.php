<?php

use Illuminate\Database\Seeder;
use App\Discussion;
use App\Comment;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->delete();

        $discussions = Discussion::all();
        $i = 1;
        foreach ($discussions as $discussion) {
            $users = $discussion->Module->Course->User;
            $discussion->Comment()->saveMany([
                new Comment(['text' => 'Comment text goes here ' . $i++, 'discussion_id' => $discussion->id, 'user_id' => $users->random(1)->id]),
                new Comment(['text' => 'Comment text goes here ' . $i++, 'discussion_id' => $discussion->id, 'user_id' => $users->random(1)->id]),
                new Comment(['text' => 'Comment text goes here ' . $i++, 'discussion_id' => $discussion->id, 'user_id' => $users->random(1)->id])
            ]);
        }
    }
}
