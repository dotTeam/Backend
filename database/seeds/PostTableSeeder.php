<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->delete();

        $faker = Faker\Factory::create();
        $forums = \App\Forum::all();
        $users = \App\User::all();
        foreach ($forums as $forum) {
            for ($i = 0; $i < 5; $i++) {
                \App\Post::create(['message' => $faker->sentence, 'forum_id' => $forum->id, 'user_id' => $users->random(1)->id]);
            }
        }
    }
}
