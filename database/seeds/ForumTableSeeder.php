<?php

use Illuminate\Database\Seeder;

class ForumTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forums')->delete();
        $users = \App\User::all();

        for ($i = 0; $i < 50; $i++) {
            \App\Forum::create(['name' => 'Forum ' . $i++, 'user_id' => $users->random(1)->id]);
        }
    }
}
