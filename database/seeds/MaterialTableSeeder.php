<?php

use Illuminate\Database\Seeder;

class MaterialTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materials')->delete();
        Storage::deleteDirectory('materials');

        $faker = Faker\Factory::create();
        $modules = \App\Module::all();
        foreach ($modules as $module) {
            $users = $module->Course->User->where('pivot.role', 'teacher');
            for ($i = 0; $i <= 4; $i++) {
                $material = \App\Material::create(['file_name' => $faker->company, 'file_extension' => $faker->fileExtension, 'description' => $faker->sentence(), 'user_id' => $users->random(1)->id, 'module_id' => $module->id]);
                Storage::put('/materials/' . $material->id, 'Test test test from materials = ' . $material->file_name . ' and id = ' . $material->id);
            }
        }
    }
}
