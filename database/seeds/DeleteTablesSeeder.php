<?php

use Illuminate\Database\Seeder;

class DeleteTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tests')->delete();
        DB::table('notification_categories')->delete();
        DB::table('comment_announcement_courses')->delete();
        DB::table('homework_answers')->delete();
        DB::table('homeworks')->delete();
        DB::table('posts')->delete();
        DB::table('comment_groups')->delete();
        DB::table('event_user')->delete();
        DB::table('events')->delete();
        DB::table('page_groups')->delete();
        DB::table('page_modules')->delete();
        DB::table('forums')->delete();
        DB::table('discussion_groups')->delete();
        DB::table('announcement_groups')->delete();
        DB::table('announcement_courses')->delete();
        DB::table('materials')->delete();
        DB::table('comments')->delete();
        DB::table('discussions')->delete();
        DB::table('modules')->delete();
        DB::table('course_user')->delete();
        DB::table('courses')->delete();
        DB::table('conversation_user')->delete();
        DB::table('messages')->delete();
        DB::table('conversations')->delete();
        DB::table('users')->delete();
        DB::table('roles')->delete();
        DB::table('groups')->delete();
        DB::table('permissions')->delete();
    }
}
