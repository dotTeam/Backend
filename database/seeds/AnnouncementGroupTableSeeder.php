<?php

use Illuminate\Database\Seeder;

class AnnouncementGroupTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('announcement_groups')->delete();

        $faker = Faker\Factory::create();
        $groups = \App\Group::all();
        foreach ($groups as $group) {
            $users = $group->User;
            if ($users->count() != 0) {
                for ($i = 0; $i < 5; $i++) {
                    \App\AnnouncementGroup::create(['title' => 'Announcement group ' . $i++, 'description' => $faker->sentence,'group_id' => $group->id, 'user_id' => $users->random(1)->id]);
                }
            }
        }
    }
}
