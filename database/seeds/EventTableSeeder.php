<?php

use Illuminate\Database\Seeder;
use App\Event;

class EventTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->delete();

        $faker = Faker\Factory::create();
        $users = \App\User::all();
        for ($i = 0; $i <= 40; $i++) {
            $teacher = $users->random(1)->first();
            $event = new Event(['name' => 'Event ' .$i, 'description' => $faker->sentence, 'frequency' => random_int(1, 2), 'start_date' => $faker->dateTimeBetween('-1 month', '+1 month'), 'end_date' => $faker->dateTimeBetween('+2 month', '+3 month'), 'user_id' => $teacher->id]);
            $random_user = $users->random(3);
            foreach($random_user as $user){
                $user->Event()->save($event);
            }
        }
    }
}
