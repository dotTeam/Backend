<?php

use Illuminate\Database\Seeder;
use App\Course;
use App\Discussion;

class DiscussionTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('discussions')->delete();

        $modules = \App\Module::all();
        $i = 1;
        foreach ($modules as $module) {
            $users = $module->Course->User;
            $module->Discussion()->saveMany([
                new Discussion(['subject' => 'Discussion ' . $i++, 'description' => 'Some description for this discussion', 'category' => 'important', 'user_id' => $users->random(1)->id]),
                new Discussion(['subject' => 'Discussion ' . $i++, 'description' => 'Some description for this discussion', 'category' => 'important', 'user_id' => $users->random(1)->id]),
                new Discussion(['subject' => 'Discussion ' . $i++, 'description' => 'Some description for this discussion', 'category' => 'arhived', 'user_id' => $users->random(1)->id])
            ]);
        }
    }
}
