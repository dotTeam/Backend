<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
class NotificationCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('notifynder:create:category', ['name' => 'store.homework', 'text' => '{from.name} added {extra.object} homework']);
        Artisan::call('notifynder:create:category', ['name' => 'update.homework', 'text' => '{from.name} updated {extra.object} homework']);

        Artisan::call('notifynder:create:category', ['name' => 'store.test', 'text' => '{from.name} added {extra.object} test']);
        Artisan::call('notifynder:create:category', ['name' => 'update.test', 'text' => '{from.name} updated {extra.object} test']);

        Artisan::call('notifynder:create:category', ['name' => 'store.event', 'text' => '{from.name} added {extra.object} event']);
        Artisan::call('notifynder:create:category', ['name' => 'update.event', 'text' => '{from.name} updated {extra.object} event']);

        Artisan::call('notifynder:create:category', ['name' => 'store.module.page', 'text' => '{from.name} added {extra.object} page from {extra.course} course']);
        Artisan::call('notifynder:create:category', ['name' => 'update.module.page', 'text' => '{from.name} updated {extra.object} page from {extra.course} course']);
    }
}
