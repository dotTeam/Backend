<?php

use Illuminate\Database\Seeder;
use App\Test;

class TestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tests')->delete();

        $modules = \App\Module::all();
        $faker = Faker\Factory::create();
        $required = [false, true];
        $j = 0;
        foreach ($modules as $module) {
            $users = $module->Course->User->where('pivot.role', 'teacher');
            $rand = random_int(0, 3);
            for ($i = 0; $i < $rand; $i++) {
                Test::create([
                    'name' => 'Test ' . $j++,
                    'description' => $faker->realText(),
                    'start_date' => $faker->dateTimeBetween('-2 month', '+1 month'),
                    'end_date' => $faker->dateTimeBetween('+2 month', '+4 month'),
                    'deadline' => $faker->dateTimeBetween('+1 month', '+2 month'),
                    'required' => $required[array_rand($required, 1)],
                    'module_id' => $module->id,
                    'user_id' => $users->random(1)->id,
                    'duration' => $faker->numberBetween(60, 3600),
                ]);
            }
        }
    }
}
