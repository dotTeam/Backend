<?php

use Illuminate\Database\Seeder;
use App\HomeworkAnswer;

class HomeworkAnswerTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('homework_answers')->delete();
        Storage::deleteDirectory('homework_answer');

        $homeworks = \App\Homework::all();
        $faker = Faker\Factory::create();

        foreach ($homeworks as $homework) {
            $users = $homework->Module->Course->User->where('pivot.role', 'student');

            foreach ($users as $user) {
                if ($homework->deadline > \Carbon\Carbon::now() && $homework->start_date < \Carbon\Carbon::now() && $homework->end_date > \Carbon\Carbon::now()) {
                    if ($homework->answer_type == 'text' || $homework->answer_type == 'link') {
                        $homework_answer = HomeworkAnswer::create([
                            'answer' => $faker->sentence,
                            'user_id' => $user->id,
                            'homework_id' => $homework->id
                        ]);
                    }
                    if ($homework->answer_type == 'upload') {
                        $homework_answer = HomeworkAnswer::create([
                            'file_name' => $faker->company,
                            'file_extension' => $faker->fileExtension,
                            'user_id' => $user->id,
                            'homework_id' => $homework->id
                        ]);
                        Storage::put('/homework_answer/' . $homework_answer->id, 'Test test test from homework answer = ' . $homework_answer->file_name . ' and id = ' . $homework_answer->id);
                    }
                }
            }
        }
    }
}
