<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Conversation;

class ConversationTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('conversations')->delete();

        $users = User::all();
        $j = 0;
        foreach ($users as $user) {
            for ($i = 0; $i < 3; $i++) {
                $attached_user = [];
                $conversation = Conversation::create(['name' => 'Conversation ' . $j++]);
                $conversation->User()->attach($user->id);
                $attached_user[] = $user->id;
                $attached_user = $this->attach_user($conversation, $attached_user, $users);
                if ($i % 2 == 0) {
                    $attached_user = $this->attach_user($conversation, $attached_user, $users);
                }
            }
        }
    }

    private function attach_user($conversation, $attached_user, $users)
    {
        $other_user = $users->random(1);
        while (in_array($other_user->id, $attached_user)) {
            $other_user = $users->random(1);
        }
        $conversation->User()->attach($other_user->id);
        $attached_user[] = $other_user->id;
        return $attached_user;
    }
}
