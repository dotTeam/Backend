<?php

use Illuminate\Database\Seeder;

class AnnouncementCourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('announcement_courses')->delete();

        $faker = Faker\Factory::create();
        $courses = \App\Course::all();
        foreach ($courses as $course) {
            $users = $course->User->where('pivot.role', 'teacher');
            if ($users->count() != 0) {
                for ($i = 0; $i < 5; $i++) {
                    \App\AnnouncementCourse::create(['title' => 'Announcement course ' . $i++, 'description' => $faker->sentence,'course_id' => $course->id, 'user_id' => $users->random(1)->id]);
                }
            }
        }
    }
}
