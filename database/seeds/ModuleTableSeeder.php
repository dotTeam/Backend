<?php

use Illuminate\Database\Seeder;
use App\Module;
use App\Course;

class ModuleTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->delete();

        $faker = Faker\Factory::create();
        $courses = Course::all();
        $i = 1;
        foreach ($courses as $cours) {
            $cours->Module()->saveMany([
                new Module(['name' => 'Module ' . $i++, 'started_at' => $faker->dateTimeBetween('-1 years', '+100 days')]),
                new Module(['name' => 'Module ' . $i++, 'started_at' => $faker->dateTimeBetween('-1 years', '+100 days')]),
                new Module(['name' => 'Module ' . $i++, 'started_at' => $faker->dateTimeBetween('-1 days', '+1 days')])
            ]);
        }
    }
}
