<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(DeleteTablesSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(GroupTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(CourseTableSeeder::class);
        $this->call(ModuleTableSeeder::class);
        $this->call(DiscussionTableSeeder::class);
        $this->call(CommentTableSeeder::class);
        $this->call(MaterialTableSeeder::class);
        $this->call(PageGroupTableSeeder::class);
        $this->call(PageModuleTableSeeder::class);
        $this->call(ForumTableSeeder::class);
        $this->call(DiscussionGroupTableSeeder::class);
        $this->call(AnnouncementGroupTableSeeder::class);
        $this->call(AnnouncementCourseTableSeeder::class);
        $this->call(PostTableSeeder::class);
        $this->call(CommentGroupTableSeeder::class);
        $this->call(ConversationTableSeeder::class);
        $this->call(MessageTableSeeder::class);
        $this->call(EventTableSeeder::class);
        $this->call(HomeworkTableSeeder::class);
        $this->call(HomeworkAnswerTableSeeder::class);
        $this->call(CommentAnnouncementCourseTableSeeder::class);
        $this->call(NotificationCategoryTableSeeder::class);
        $this->call(TestTableSeeder::class);

        Model::reguard();
    }
}
