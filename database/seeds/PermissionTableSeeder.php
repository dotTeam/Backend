<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class PermissionTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();

        $admin = Role::where('role', 'admin')->first();
        $teacher = Role::where('role', 'teacher')->first();
        $student = Role::where('role', 'student')->first();

        // Comment
        //$permission = Permission::create(['route_name' => 'api.v1.comment.store', 'description' => 'Some description ...', 'model' => 'comment']);

        $permission = Permission::create(['route_name' => 'api.v1.comment.update', 'description' => 'Some description ...', 'model' => 'comment']);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($admin->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.comment.destroy', 'description' => 'Some description ...', 'model' => 'comment']);
        $permission->Role()->attach($admin->id);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        // Course
        $permission = Permission::create(['route_name' => 'api.v1.course.index', 'description' => 'Some description ...', 'model' => 'course', 'check' => false]);

        //$permission = Permission::create(['route_name' => 'api.v1.course.update', 'description' => 'Some description ...', 'model' => 'course']);

        $permission = Permission::create(['route_name' => 'api.v1.course.store', 'description' => 'Some description ...', 'model' => 'course']);
        $permission->Role()->attach($teacher->id);

        //$permission = Permission::create(['route_name' => 'api.v1.course.destroy', 'description' => 'Some description ...', 'model' => 'course', 'check' => false]);

        $permission = Permission::create(['route_name' => 'api.v1.module.discussion.index', 'description' => 'Some description ...', 'model' => 'discussion', 'check' => false]);
        $permission = Permission::create(['route_name' => 'api.v1.course.module.index', 'description' => 'Some description ...', 'model' => 'module', 'check' => false]);

        //Course user
        $permission = Permission::create(['route_name' => 'api.v1.user.course.index', 'description' => 'Some description ...', 'model' => '', 'check' => false]);

        $permission = Permission::create(['route_name' => 'api.v1.course.user.index', 'description' => 'Some description ...', 'model' => '', 'check' => false]);

        //$permission = Permission::create(['route_name' => 'api.v1.course.user.store', 'description' => 'Some description ...', 'model' => '']);
        //$permission = Permission::create(['route_name' => 'api.v1.course.user.update', 'description' => 'Some description ...', 'model' => '']);
        //$permission = Permission::create(['route_name' => 'api.v1.course.user.destroy', 'description' => 'Some description ...', 'model' => '']);

        // Discussion
        //$permission = Permission::create(['route_name' => 'api.v1.discussion.store', 'description' => 'Some description ...', 'model' => 'discussion']);

        $permission = Permission::create(['route_name' => 'api.v1.discussion.update', 'description' => 'Some description ...', 'model' => 'discussion']);
        $permission->Role()->attach($admin->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.discussion.destroy', 'description' => 'Some description ...', 'model' => 'discussion']);
        $permission->Role()->attach($admin->id);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.discussion.comment.index', 'description' => 'Some description ...', 'model' => 'discussion', 'check' => false]);

        // Group
        $permission = Permission::create(['route_name' => 'api.v1.group.index', 'description' => 'Some description ...', 'model' => 'group', 'check' => false]);

        $permission = Permission::create(['route_name' => 'api.v1.group.store', 'description' => 'Some description ...', 'model' => 'group']);
        $permission->Role()->attach($admin->id);

        $permission = Permission::create(['route_name' => 'api.v1.group.update', 'description' => 'Some description ...', 'model' => 'group']);
        $permission->Role()->attach($admin->id);

        $permission = Permission::create(['route_name' => 'api.v1.group.destroy', 'description' => 'Some description ...', 'model' => 'group']);
        $permission->Role()->attach($admin->id);

        // Log
        $permission = Permission::create(['route_name' => 'api.v1.log.delete', 'description' => 'Some description ...', 'model' => '']);
        $permission->Role()->attach($admin->id);

        $permission = Permission::create(['route_name' => 'api.v1.log.index', 'description' => 'Some description ...', 'model' => '']);
        $permission->Role()->attach($admin->id);


        // Modules
        //$permission = Permission::create(['route_name' => 'api.v1.module.store', 'description' => 'Some description ...', 'model' => 'module']);
        //$permission = Permission::create(['route_name' => 'api.v1.module.update', 'description' => 'Some description ...', 'model' => 'module']);
        //$permission = Permission::create(['route_name' => 'api.v1.module.destroy', 'description' => 'Some description ...', 'model' => 'module']);

        // PageModule
        $permission = Permission::create(['route_name' => 'api.v1.module.page.index', 'description' => 'Some description ...', 'model' => 'PageModule', 'check' => false]);

        //$permission = Permission::create(['route_name' => 'api.v1.page-module.store', 'description' => 'Some description ...', 'model' => 'PageModule']);

        $permission = Permission::create(['route_name' => 'api.v1.page-module.update', 'description' => 'Some description ...', 'model' => 'PageModule']);
        $permission->Role()->attach($admin->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.page-module.destroy', 'description' => 'Some description ...', 'model' => 'PageModule']);
        $permission->Role()->attach($admin->id);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);

        // Test
        $permission = Permission::create(['route_name' => 'api.v1.module.test.index', 'description' => 'Some description ...', 'model' => 'test', 'check' => false]);

        //$permission = Permission::create(['route_name' => 'api.v1.test.store', 'description' => 'Some description ...', 'model' => 'test']);

        $permission = Permission::create(['route_name' => 'api.v1.test.update', 'description' => 'Some description ...', 'model' => 'test']);
        $permission->Role()->attach($admin->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.test.destroy', 'description' => 'Some description ...', 'model' => 'test']);
        $permission->Role()->attach($admin->id);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);

        // Question Answer
        //$permission = Permission::create(['route_name' => 'api.v1.test.question-answer.index', 'description' => 'Some description ...', 'model' => 'QuestionAnswer']);
        //$permission->Role()->attach($admin->id);
        //$permission->Role()->attach($teacher->id);

        // Homework
        $permission = Permission::create(['route_name' => 'api.v1.module.homework.index', 'description' => 'Some description ...', 'model' => 'homework', 'check' => false]);

        //$permission = Permission::create(['route_name' => 'api.v1.homework.store', 'description' => 'Some description ...', 'model' => 'homework']);

        $permission = Permission::create(['route_name' => 'api.v1.homework.update', 'description' => 'Some description ...', 'model' => 'homework']);
        $permission->Role()->attach($admin->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.homework.destroy', 'description' => 'Some description ...', 'model' => 'homework']);
        $permission->Role()->attach($admin->id);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);

        // HomeworkAnswer
        $permission = Permission::create(['route_name' => 'api.v1.homework.homework-answer.index', 'description' => 'Some description ...', 'model' => 'HomeworkAnswer', 'check' => false]);

        //$permission = Permission::create(['route_name' => 'api.v1.homework.homework-answer.store', 'description' => 'Some description ...', 'model' => 'HomeworkAnswer']);

        $permission = Permission::create(['route_name' => 'api.v1.homework.homework-answer.destroy', 'description' => 'Some description ...', 'model' => 'HomeworkAnswer']);
        $permission->Role()->attach($admin->id);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        // Material
        $permission = Permission::create(['route_name' => 'api.v1.module.material.download', 'description' => 'Some description ...', 'model' => 'material', 'check' => false]);

        $permission = Permission::create(['route_name' => 'api.v1.module.material.index', 'description' => 'Some description ...', 'model' => 'material', 'check' => false]);

        //$permission = Permission::create(['route_name' => 'api.v1.material.store', 'description' => 'Some description ...', 'model' => 'material']);

        $permission = Permission::create(['route_name' => 'api.v1.material.update', 'description' => 'Some description ...', 'model' => 'material']);
        $permission->Role()->attach($admin->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.material.destroy', 'description' => 'Some description ...', 'model' => 'material']);
        $permission->Role()->attach($admin->id);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);


        // Roles
        $permission = Permission::create(['route_name' => 'api.v1.role.index', 'description' => 'Some description ...', 'model' => 'role']);
        $permission->Role()->attach($admin->id);

        // PageGroup
        $permission = Permission::create(['route_name' => 'api.v1.group.page.index', 'description' => 'Some description ...', 'model' => 'PageGroup', 'check' => false]);

        //$permission = Permission::create(['route_name' => 'api.v1.page-group.store', 'description' => 'Some description ...', 'model' => 'PageGroup']);

        $permission = Permission::create(['route_name' => 'api.v1.page-group.update', 'description' => 'Some description ...', 'model' => 'PageGroup']);
        $permission->Role()->attach($admin->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.page-group.destroy', 'description' => 'Some description ...', 'model' => 'PageGroup']);
        $permission->Role()->attach($admin->id);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        // Forum
        $permission = Permission::create(['route_name' => 'api.v1.forum.index', 'description' => 'Some description ...', 'model' => 'forum', 'check' => false]);

        $permission = Permission::create(['route_name' => 'api.v1.forum.store', 'description' => 'Some description ...', 'model' => 'forum', 'check' => false]);

        $permission = Permission::create(['route_name' => 'api.v1.forum.update', 'description' => 'Some description ...', 'model' => 'forum']);
        $permission->Role()->attach($admin->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.forum.destroy', 'description' => 'Some description ...', 'model' => 'forum']);
        $permission->Role()->attach($admin->id);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        // Discussion Group
        $permission = Permission::create(['route_name' => 'api.v1.group.discussion-group.index', 'description' => 'Some description ...', 'model' => 'DiscussionGroup', 'check' => false]);

        //$permission = Permission::create(['route_name' => 'api.v1.discussion-group.store', 'description' => 'Some description ...', 'model' => 'DiscussionGroup']);

        $permission = Permission::create(['route_name' => 'api.v1.discussion-group.update', 'description' => 'Some description ...', 'model' => 'DiscussionGroup']);
        $permission->Role()->attach($admin->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.discussion-group.destroy', 'description' => 'Some description ...', 'model' => 'DiscussionGroup']);
        $permission->Role()->attach($admin->id);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        // Comment Group
        $permission = Permission::create(['route_name' => 'api.v1.discussion-group.comment-group.index', 'description' => 'Some description ...', 'model' => 'CommentGroup', 'check' => false]);

        //$permission = Permission::create(['route_name' => 'api.v1.comment-group.store', 'description' => 'Some description ...', 'model' => 'CommentGroup']);

        $permission = Permission::create(['route_name' => 'api.v1.comment-group.update', 'description' => 'Some description ...', 'model' => 'CommentGroup']);
        $permission->Role()->attach($admin->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.comment-group.destroy', 'description' => 'Some description ...', 'model' => 'CommentGroup']);
        $permission->Role()->attach($admin->id);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);


        // Announcement Group
        $permission = Permission::create(['route_name' => 'api.v1.announcement-group.index', 'description' => 'Some description ...', 'model' => 'AnnouncementGroup', 'check' => false]);

        //$permission = Permission::create(['route_name' => 'api.v1.announcement-group.store', 'description' => 'Some description ...', 'model' => 'AnnouncementGroup']);

        $permission = Permission::create(['route_name' => 'api.v1.announcement-group.update', 'description' => 'Some description ...', 'model' => 'AnnouncementGroup']);
        $permission->Role()->attach($admin->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.announcement-group.destroy', 'description' => 'Some description ...', 'model' => 'AnnouncementGroup']);
        $permission->Role()->attach($admin->id);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        // Announcement Course
        $permission = Permission::create(['route_name' => 'api.v1.announcement-course.index', 'description' => 'Some description ...', 'model' => 'AnnouncementCourse', 'check' => false]);

        //$permission = Permission::create(['route_name' => 'api.v1.announcement-course.store', 'description' => 'Some description ...', 'model' => 'AnnouncementCourse']);

        $permission = Permission::create(['route_name' => 'api.v1.announcement-course.update', 'description' => 'Some description ...', 'model' => 'AnnouncementCourse']);
        $permission->Role()->attach($admin->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.announcement-course.destroy', 'description' => 'Some description ...', 'model' => 'AnnouncementCourse']);
        $permission->Role()->attach($admin->id);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        // Comment Announcement Course
        $permission = Permission::create(['route_name' => 'api.v1.announcement-course.comment.index', 'description' => 'Some description ...', 'model' => 'CommentAnnouncementCourse', 'check' => false]);

        //$permission = Permission::create(['route_name' => 'api.v1.comment-announcement-course.store', 'description' => 'Some description ...', 'model' => 'CommentAnnouncementCourse']);

        $permission = Permission::create(['route_name' => 'api.v1.comment-announcement-course.update', 'description' => 'Some description ...', 'model' => 'CommentAnnouncementCourse']);
        $permission->Role()->attach($admin->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.comment-announcement-course.destroy', 'description' => 'Some description ...', 'model' => 'CommentAnnouncementCourse']);
        $permission->Role()->attach($admin->id);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        // Post
        $permission = Permission::create(['route_name' => 'api.v1.forum.post.index', 'description' => 'Some description ...', 'model' => 'post', 'check' => false]);

        $permission = Permission::create(['route_name' => 'api.v1.post.store', 'description' => 'Some description ...', 'model' => 'post', 'check' => false]);

        $permission = Permission::create(['route_name' => 'api.v1.post.update', 'description' => 'Some description ...', 'model' => 'post']);
        $permission->Role()->attach($admin->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.post.destroy', 'description' => 'Some description ...', 'model' => 'post']);
        $permission->Role()->attach($admin->id);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        // Event
        $permission = Permission::create(['route_name' => 'api.v1.event.index', 'description' => 'Some description ...', 'model' => 'event', 'check' => false]);

        //$permission = Permission::create(['route_name' => 'api.v1.event.store', 'description' => 'Some description ...', 'model' => 'event']);

        $permission = Permission::create(['route_name' => 'api.v1.event.update', 'description' => 'Some description ...', 'model' => 'event']);
        $permission->Role()->attach($admin->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($teacher->id, ['if_created_by_auth' => true]);
        $permission->Role()->attach($student->id, ['if_created_by_auth' => true]);

        $permission = Permission::create(['route_name' => 'api.v1.event.destroy', 'description' => 'Some description ...', 'model' => 'event', 'check' => false]);

        // User
        $permission = Permission::create(['route_name' => 'api.v1.user.index', 'description' => 'Some description ...', 'model' => 'user', 'check' => false]);

        $permission = Permission::create(['route_name' => 'api.v1.user.store', 'description' => 'Some description ...', 'model' => 'user']);
        $permission->Role()->attach($admin->id);

        //$permission = Permission::create(['route_name' => 'api.v1.user.update', 'description' => 'Some description ...', 'model' => 'user']);

        //$permission = Permission::create(['route_name' => 'api.v1.user.destroy', 'description' => 'Some description ...', 'model' => 'user']);

        // Conversation
        $permission = Permission::create(['route_name' => 'api.v1.conversation.index', 'description' => 'Some description ...', 'model' => 'conversation', 'check' => false]);
        $permission = Permission::create(['route_name' => 'api.v1.conversation.destroy', 'description' => 'Some description ...', 'model' => 'conversation', 'check' => false]);

        // Message
        $permission = Permission::create(['route_name' => 'api.v1.message.index', 'description' => 'Some description ...', 'model' => 'user', 'check' => false]);
        $permission = Permission::create(['route_name' => 'api.v1.message.store', 'description' => 'Some description ...', 'model' => 'user', 'check' => false]);

    }
}
