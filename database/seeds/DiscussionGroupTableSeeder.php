<?php

use Illuminate\Database\Seeder;

class DiscussionGroupTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('discussion_groups')->delete();

        $groups = \App\Group::all();
        foreach ($groups as $group) {
            $users = $group->User;
            if ($users->count() != 0) {
                for ($i = 0; $i < 5; $i++) {
                    \App\DiscussionGroup::create(['name' => 'Discussion group ' . $i++, 'group_id' => $group->id, 'user_id' => $users->random(1)->id]);
                }
            }
        }
    }
}
