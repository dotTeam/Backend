<?php

use Illuminate\Database\Seeder;
use App\Message;

class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messages')->delete();

        $faker = Faker\Factory::create();
        $conversations = \App\Conversation::all();
        foreach($conversations as $conversation){
            $users = $conversation->User;
            for($i = 0; $i <= 10; $i++) {
                $message = new Message(['message' => $faker->sentence, 'user_id' => $users->random(1)->id, 'created_at' => $faker->dateTimeBetween('-1 year', 'now'), 'conversation_id' => $conversation->id]);
                DB::table('messages')->insert($message->toArray());
            }
        }
    }
}
