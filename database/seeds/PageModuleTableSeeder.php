<?php

use Illuminate\Database\Seeder;
use App\PageModule;
class PageModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('page_modules')->delete();

        $modules = \App\Module::all();
        $i = 1;
        foreach ($modules as $module) {
            $users = $module->Course->User->where('pivot.role', 'teacher');
            $module->PageModule()->saveMany([
                new PageModule(['name' => 'Page ' . $i++, 'content' => "<h3>Title</h3><p>Hey, I'am a pharagraph</p>", 'user_id' => $users->random(1)->id]),
                new PageModule(['name' => 'Page ' . $i++, 'content' => "<b>hey hou</b><p>Hey, I'am a pharagraph</p>", 'user_id' => $users->random(1)->id]),
                new PageModule(['name' => 'Page ' . $i++, 'content' => "<p>Hey, I'am a pharagraph</p><ul><li>list 1</li><li>list 2</li></ul>", 'user_id' => $users->random(1)->id])
            ]);
        }
    }
}
