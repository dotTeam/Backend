<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Group;
use Illuminate\Support\Facades\Storage;

class UserTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        Storage::deleteDirectory('profile_img');

        $admin = Role::where('role', 'admin')->first();
        $teacher = Role::where('role', 'teacher')->first();
        $student = Role::where('role', 'student')->first();

        $group_123 = Group::where('name', '123')->first();
        $group_124 = Group::where('name', '124')->first();

        $randomuser_json = json_decode(file_get_contents("https://randomuser.me/api/?results=7"));
        $users = [];

        // Admins
        $users[] = User::create(['name' => 'Admin 1', 'email' => 'user1@example.com', 'password' => Hash::make('123456'), 'first_name' => 'first1', 'last_name' => 'last1', 'registration_number' => '123', 'role_id' => $admin->id]);
        $users[] = User::create(['name' => 'Admin 2', 'email' => 'user2@example.com', 'password' => Hash::make('123456'), 'first_name' => 'first2', 'last_name' => 'last2', 'registration_number' => '234', 'role_id' => $admin->id, 'use_gravatar' => true]);

        // Teachers
        $users[] = User::create(['name' => 'Teacher 1', 'email' => 'user3@example.com', 'password' => Hash::make('123456'), 'first_name' => 'first3', 'last_name' => 'last3', 'registration_number' => '345', 'role_id' => $teacher->id]);
        $users[] = User::create(['name' => 'Teacher 2', 'email' => 'user4@example.com', 'password' => Hash::make('123456'), 'first_name' => 'first4', 'last_name' => 'last4', 'registration_number' => '456', 'role_id' => $teacher->id, 'use_gravatar' => true]);

        // Students
        $users[] = User::create(['name' => 'Student 1', 'email' => 'user5@example.com', 'password' => Hash::make('123456'), 'first_name' => 'first5', 'last_name' => 'last5', 'registration_number' => '567', 'role_id' => $student->id, 'group_id' => $group_123->id]);
        $users[] = User::create(['name' => 'Student 2', 'email' => 'user6@example.com', 'password' => Hash::make('123456'), 'first_name' => 'first6', 'last_name' => 'last6', 'registration_number' => '678', 'role_id' => $student->id, 'group_id' => $group_123->id]);
        $users[] = User::create(['name' => 'Student 3', 'email' => 'user7@example.com', 'password' => Hash::make('123456'), 'first_name' => 'first7', 'last_name' => 'last7', 'registration_number' => '789', 'role_id' => $student->id, 'group_id' => $group_124->id, 'use_gravatar' => true]);

        foreach ($users as $key => $user) {
            if(random_int(0, 1)){
                Storage::put('/profile_img/' . $user->id, file_get_contents($randomuser_json->results[$key]->picture->medium));
            }
        }
    }
}