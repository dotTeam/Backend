<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        Role::create(['role' => 'admin']);
        Role::create(['role' => 'teacher']);
        Role::create(['role' => 'student']);
    }
}
