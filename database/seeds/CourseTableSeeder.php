<?php

use Illuminate\Database\Seeder;
use App\Course;
use App\User;

class CourseTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('course_user')->delete();
        DB::table('courses')->delete();

        $teacher_1 = User::where('name', 'Teacher 1')->first();
        $teacher_2 = User::where('name', 'Teacher 2')->first();

        $student_1 = User::where('name', 'Student 1')->first();
        $student_2 = User::where('name', 'Student 2')->first();
        $student_3 = User::where('name', 'Student 3')->first();

        $cours_1 = Course::create(['name' => 'SOD', 'description' => 'Some description for ...']);
        $cours_2 = Course::create(['name' => 'Aspecte']);
        $cours_3 = Course::create(['name' => 'SO', 'description' => 'Some description for ...']);
        $cours_4 = Course::create(['name' => 'Programare Web']);

        $cours_5 = Course::create(['name' => 'Gestiunea proiectelor soft', 'description' => 'Some description for ...']);
        $cours_6 = Course::create(['name' => 'ASC', 'description' => 'Some description for ...']);
        $cours_7 = Course::create(['name' => 'MAP', 'description' => 'Some description for ...']);
        $cours_8 = Course::create(['name' => 'Baze de date']);

        $teacher_1->Course()->attach($cours_1->id, ['role' => 'teacher']);
        $teacher_1->Course()->attach($cours_2->id, ['role' => 'teacher']);
        $teacher_1->Course()->attach($cours_3->id, ['role' => 'teacher']);
        $teacher_1->Course()->attach($cours_4->id, ['role' => 'teacher']);

        $teacher_2->Course()->attach($cours_5->id, ['role' => 'teacher']);
        $teacher_2->Course()->attach($cours_6->id, ['role' => 'teacher']);
        $teacher_2->Course()->attach($cours_7->id, ['role' => 'teacher']);
        $teacher_2->Course()->attach($cours_8->id, ['role' => 'teacher']);

        $student_1->Course()->attach($cours_1->id, ['role' => 'student']);
        $student_1->Course()->attach($cours_2->id, ['role' => 'student']);

        $student_2->Course()->attach($cours_3->id, ['role' => 'student']);
        $student_2->Course()->attach($cours_4->id, ['role' => 'student']);

        $student_3->Course()->attach($cours_1->id, ['role' => 'student']);
        $student_3->Course()->attach($cours_2->id, ['role' => 'student']);
        $student_3->Course()->attach($cours_7->id, ['role' => 'student']);
    }
}
