<?php

use Illuminate\Database\Seeder;

class CommentAnnouncementCourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comment_announcement_courses')->delete();

        $faker = Faker\Factory::create();
        $announcement_courses = \App\AnnouncementCourse::all();
        foreach ($announcement_courses as $announcement_cours) {
            $users = $announcement_cours->Course->User;
            if ($users->count() != 0) {
                for ($i = 0; $i < 5; $i++) {
                    \App\CommentAnnouncementCourse::create(['text' => $faker->sentence, 'announcement_course_id' => $announcement_cours->id, 'user_id' => $users->random(1)->id]);
                }
            }
        }
    }
}
