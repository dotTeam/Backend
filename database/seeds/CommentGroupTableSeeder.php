<?php

use Illuminate\Database\Seeder;

class CommentGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comment_groups')->delete();

        $faker = Faker\Factory::create();
        $discussion_groups = \App\DiscussionGroup::all();
        foreach ($discussion_groups as $discussion_group) {
            $users = $discussion_group->Group->User;
            if ($users->count() != 0) {
                for ($i = 0; $i < 5; $i++) {
                    \App\CommentGroup::create(['message' => $faker->sentence, 'discussion_group_id' => $discussion_group->id, 'user_id' => $users->random(1)->id]);
                }
            }
        }
    }
}
