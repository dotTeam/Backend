<?php

use Illuminate\Database\Seeder;
use App\Group;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->delete();

        Group::create(['name' => '123', 'description' => 'Some description for group 123']);
        Group::create(['name' => '124', 'description' => '']);
        Group::create(['name' => '125', 'description' => 'Some description for group 125']);
    }
}
