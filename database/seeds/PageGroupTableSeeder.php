<?php

use Illuminate\Database\Seeder;

class PageGroupTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('page_groups')->delete();

        $faker = Faker\Factory::create();
        $groups = \App\Group::all();
        foreach ($groups as $group) {
            $users = $group->User;
            if ($users->count() != 0) {
                for ($i = 0; $i < 5; $i++) {
                    \App\PageGroup::create(['name' => 'Page ' . $i++, 'content' => $faker->sentence, 'group_id' => $group->id, 'user_id' => $users->random(1)->id]);
                }
            }
        }
    }
}
