<?php

use Illuminate\Database\Seeder;
use App\Homework;

class HomeworkTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('homeworks')->delete();

        $modules = \App\Module::all();
        $faker = Faker\Factory::create();
        $type = ['type 1', 'type 2'];
        $required = [false, true];
        $answer_type = ['text', 'link', 'upload', 'physical_format'];
        $j = 0;
        foreach ($modules as $module) {
            $users = $module->Course->User->where('pivot.role', 'teacher');
            $rand = random_int(0, 3);
            for ($i = 0; $i < $rand; $i++) {
                Homework::create([
                    'name' => 'Homework ' . $j++,
                    'description' => $faker->realText(),
                    'start_date' => $faker->dateTimeBetween('-2 month', '+1 month'),
                    'end_date' => $faker->dateTimeBetween('+2 month', '+4 month'),
                    'deadline' => $faker->dateTimeBetween('+1 month', '+2 month'),
                    'points' => rand(0, 10),
                    'type' => $type[array_rand($type, 1)],
                    'required' => $required[array_rand($required, 1)],
                    'answer_type' => $answer_type[array_rand($answer_type, 1)],
                    'module_id' => $module->id,
                    'user_id' => $users->random(1)->id,
                ]);
            }
        }
    }
}
