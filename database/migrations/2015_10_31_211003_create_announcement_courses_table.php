<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcement_courses', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title', 60);
            $table->string('description', 60)->nullable();
            $table->unsignedInteger('course_id');
            $table->unsignedInteger('user_id');

            $table->timestamp('created_at');

            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('announcement_courses');
    }
}
