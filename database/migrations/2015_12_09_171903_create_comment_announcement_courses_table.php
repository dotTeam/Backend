<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentAnnouncementCoursesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_announcement_courses', function (Blueprint $table) {
            $table->increments('id');

            $table->string('text');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('announcement_course_id');

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('announcement_course_id')->references('id')->on('announcement_courses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comment_announcement_courses');
    }
}
