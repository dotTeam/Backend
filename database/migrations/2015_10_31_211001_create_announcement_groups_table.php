<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcement_groups', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title', 60);
            $table->string('description', 60)->nullable();
            $table->unsignedInteger('group_id');
            $table->unsignedInteger('user_id');

            $table->timestamp('created_at');

            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('announcement_groups');
    }
}
