<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');

            $table->string('question');
            $table->string('description')->nullable();
            $table->json('answer');
            $table->json('correct_answer');
            //$table->double('value');
            //$table->enum('type', ['descriptiveText', 'fileUpload', 'text', 'boolean', 'answerPick', 'textGaps', 'multipleTextGaps', 'multipleAnswers', 'valueAssociations', 'dynamicallyGeneratedQuestion', 'numericAnswer']);
            $table->unsignedInteger('test_id');

            $table->timestamps();

            $table->foreign('test_id')->references('id')->on('tests')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questions');
    }
}
