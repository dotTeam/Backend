<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grades', function (Blueprint $table) {
            $table->increments('id');

            $table->float('grade');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('discussion_id')->nullable();
            $table->unsignedInteger('test_id')->nullable();
            $table->unsignedInteger('homework_answer_id')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('discussion_id')->references('id')->on('discussions')->onDelete('set null');
            $table->foreign('test_id')->references('id')->on('tests')->onDelete('set null');
            $table->foreign('homework_answer_id')->references('id')->on('homework_answers')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('grades');
    }
}
